#!/bin/bash
LANG=rus #replace with your language code

shopt -s nullglob

tesseract -psm 1 -l $LANG $1 $1 pdf

echo "Joining files into single PDF..."
pdftk *.pdf cat output ../outdocument.pdf
rm -r -f *.pdf
