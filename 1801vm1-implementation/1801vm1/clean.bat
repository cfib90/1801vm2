@echo off
if exist async\out 				rd  async\out /s /q
if exist async\syn\quartus\db 			rd  async\syn\quartus\db /s /q
if exist async\syn\quartus\incremental_db 	rd  async\syn\quartus\incremental_db /s /q
if exist async\sim\rtl_work	 		rd  async\sim\rtl_work /s /q

if exist qsync\out 				rd  qsync\out /s /q
if exist qsync\syn\quartus\db 			rd  qsync\syn\quartus\db /s /q
if exist qsync\syn\quartus\incremental_db 	rd  qsync\syn\quartus\incremental_db /s /q
if exist qsync\sim\rtl_work		 	rd  qsync\sim\rtl_work /s /q

if exist wsync\out 				rd  wsync\out /s /q
if exist wsync\syn\quartus\db 			rd  wsync\syn\quartus\db /s /q
if exist wsync\syn\quartus\incremental_db 	rd  wsync\syn\quartus\incremental_db /s /q
if exist wsync\sim\rtl_work	 		rd  wsync\sim\rtl_work /s /q

if exist wsync\tbe\de0_test.mif 		del wsync\tbe\de0_test.mif /q
del *.sdo *.vo *.sft *.wlf *.ver *.mem *.xrf *.bak msim_transcript. vm1_run_msim*.do modelsim.ini *.rpt /q /s
@echo on
