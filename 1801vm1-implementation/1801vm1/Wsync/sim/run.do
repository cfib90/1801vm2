transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+../tbe {../rtl/vm1_wb.v}
vlog -vlog01compat -work work +incdir+../tbe {../rtl/vm1_alib.v}
vlog -vlog01compat -work work +incdir+../tbe {../rtl/vm1_tve.v}
vlog -vlog01compat -work work +incdir+../tbe {../rtl/vm1_plm.v}
vlog -vlog01compat -work work +incdir+../tbe {../tbe/config.v}
vlog -vlog01compat -work work +incdir+../tbe {../tbe/de0_alib.v}
vlog -vlog01compat -work work +incdir+../tbe {../tbe/de0_top.v}
vlog -vlog01compat -work work +incdir+../tbe {../tbe/de0_tb1.v}
vlog -vlog01compat -work work +incdir+../tbe {../tbe/de0_uart.v}
vlog -vlog01compat -work work +incdir+../tbe {../tbe/de0_vic.v}
vlog -vlog01compat -work work +incdir+../syn/quartus/db {../syn/quartus/db/de0_pll100_altpll.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneiii_ver -L rtl_work -L work -voptargs="+acc"  tb1

do wave.do
view structure
view signals
run -all
