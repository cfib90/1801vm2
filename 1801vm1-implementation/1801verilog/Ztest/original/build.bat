@echo off
copy %1.mac %pf_tmp%\%1.mac >>NUL
echo macro hd2:%1.mac /list:hd2:%1.lst /object:hd2:%1.obj >%pf_tmp%\build.com
echo link hd2:%1.obj /execute:hd2:%1.lda /lda >>%pf_tmp%\build.com
%pf_ecc_pdp11%\pdp11.exe @hd2:build.com
srec_cat %pf_tmp%\%1.lda -dec_binary -o %pf_tmp%\%1.bin -binary
move vt52.log %pf_tmp%\vt52.log
fc /b %pf_tmp%\%1.bin original\%1.bin > %pf_tmp%\%1.a
srec_cat %pf_tmp%\%1.bin -binary -byte-swap 2 -o %pf_tmp%\%1.mem --VMem 16
copy %pf_tmp%\%1.mem ..\simulation\modelsim\test.mem
@echo on
