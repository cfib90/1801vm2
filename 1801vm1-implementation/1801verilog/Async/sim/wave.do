onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb2/clk
add wave -noupdate -group Testbech /tb2/clko
add wave -noupdate -group Testbech /tb2/dclo
add wave -noupdate -group Testbech /tb2/aclo
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/pin_clk
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/vm_clk_p1
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/vm_clk_p2
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/vm_clk_p3
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/vm_clk_p4
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/f1
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/cpu/core/f2
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/clko
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/dclo
add wave -noupdate -expand -group Processor -expand -group {Reset & Clock} /tb2/aclo
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {200 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {274400 ps}
