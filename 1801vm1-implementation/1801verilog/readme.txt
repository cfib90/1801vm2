��������� �������:

\Async - ����������� ������, ����������� ���������� � �����, ������������ ������ ��� �������������.
         ��� ������������� ����� ���������� ��������, ��� ��� ������ �� �������� �������� ��������
	 � �� ��������� ������ ����������� ����� �������������� "������", ���������� ������ ������������.
         ������ ������ �������� � ����� ��� ����, ����������� ��������������� ���������. ��������
	 ������ ������ ����� ��� ������������� � FPGA ����������. ������� ��������� � ������ ������
	 �������� �� �����, ����� ����������� ������������ �������� � ������ ������.

\Qsync - ���������� ������, � ����� Qbus, ������������ ������ ��� �������������, �� �������� �������������
	 � ��� ����� ���� ���������� ��� ������������� �� FPGA. �������� ���������� ������ ����� - �������������
	 �� ������������� ����� ����������. ������ ��� - ��� "���������" ������, ����� ������� � ��������� �
	 ����������� ��������� ��������� �������������, ������� ������ ������������ ���� Qbus. � ������ ������
	 ����� �������� ����������� ���������� � ����������� ����������� ����������� ������.

\Wsync - ���������� ������, � ����� Wishbone, ������������� ��� ������������� � FPGA. �������� ��������
	 ���������� � ����������� �������, ������������ � ������� �������� �� FPGA

\Ztest - ����������� ����������� ��� ������������ ����������, ��������� ����� 791401 � 791404, ���
	 ������������ ��������������� ������, ��� � ��������� ��� ���������� � ������ � �� DE0

�������� ���������� �������� ������ Wsync (������������� � FPGA)
