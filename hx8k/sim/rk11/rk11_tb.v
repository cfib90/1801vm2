module rk11_tb ();
    
    rk11 DUT (
        .clock(clock),
        .reset(reset),
        .ce(ce),
            // -- qbus slave
        .slave_write(slave_write),
        .slave_read(slave_read),
        .slave_write_byte(slave_write_byte),
        .slave_din(slave_din),
        .slave_dout(slave_dout),
        .slave_address(slave_address),
        .slave_wdata(slave_wdata),
        .slave_rdata(slave_rdata),
        .slave_rply(slave_rply),
        .slave_addr_match(slave_addr_match),
            
            // -- qbus master
        .master_dmr(master_dmr),
        .master_dmgo(master_dmgo),
        .master_sack(master_sack),
        .master_ad(master_ad),
        .master_rdata(master_rdata),
        .master_dout(master_dout),
        .master_din(master_din),
        .master_wtbt(master_wtbt),
        .master_sync(master_sync),
        .master_ar(master_ar),
        .master_rply(master_rply),
            
            // -- interrupt
        .irq(irq),
        .irq_vector(irq_vector),
        .iako_rply(iako_rply),
        .iako(iako),
            
            // -- Serial I/O
        .tx_data(tx_data),
        .tx_valid(tx_valid),
        .tx_busy(tx_busy),
            
        .rx_data(rx_data),
        .rx_valid(rx_valid)
    );


    reg clock;
    reg reset;
    wire ce;
    // -- qbus slave
    reg         slave_write;
    reg         slave_read;
    reg         slave_write_byte;
    reg         slave_din;
    reg         slave_dout;
    reg [15:0]  slave_address;
    reg [15:0]  slave_wdata;
    wire  [15:0]  slave_rdata;
    wire         slave_rply;
    wire         slave_addr_match;
    
    // -- qbus master
    wire         master_dmr;
    reg         master_dmgo;
    wire         master_sack;
    wire [15:0]   master_ad;
    reg [15:0]  master_rdata;
    wire         master_dout;
    wire         master_din;
    wire         master_wtbt;
    wire         master_sync;
    reg         master_ar;
    reg         master_rply;
    
    // -- interrupt
    wire          irq;
    wire [15:0]  irq_vector;
    wire          iako_rply;
    reg         iako;
    
    // -- Serial I/O
    wire  [7:0]   tx_data;
    wire          tx_valid;
    reg           tx_busy;
    
    reg  [7:0]   rx_data;
    reg          rx_valid;
    
    
    reg [3:0] ce_count;
    assign ce = &ce_count;
    
    always @(posedge clock) begin
        ce_count <= ce_count + 1;
    end
    
    always
        #1 clock <= ~clock;
    
    
    task WRITE_SLAVE;
        input [15:0] addr;
        input [15:0] data;
        begin
            slave_write   <= 1'b1;
            slave_address <= addr;
            slave_wdata <= data;
            slave_dout  <= 1'b1;
            @(posedge ce);
            slave_write   <= 1'b0;
            if (~slave_rply)
                @(posedge slave_rply);
            @(posedge ce);
            slave_dout  <= 1'b0;
        end
    endtask
    
    
    localparam ADDRESS_RKDS = 16'o177400;
    localparam ADDRESS_RKER = 16'o177402;
    localparam ADDRESS_RKCS = 16'o177404;
    localparam ADDRESS_RKWC = 16'o177406;
    localparam ADDRESS_RKBA = 16'o177410;
    localparam ADDRESS_RKDA = 16'o177412;
    localparam ADDRESS_RKMR = 16'o177414;
    localparam ADDRESS_RKDB = 16'o177416;
    
    always @(posedge master_dmr) begin
        @(posedge ce);
        master_dmgo <= 1'b1;
        @(negedge master_dmr);
        master_dmgo <= 1'b0;
    end
    
    always @(posedge master_sync) begin
        @(posedge ce);
        master_ar <= 1'b1;
        master_address <= master_ad;
        @(posedge master_dout or posedge master_din)
        master_ar <= 1'b0;
    end
    
    reg [15:0] master_address;
    
    always @(posedge master_din or posedge master_dout) begin
        @(posedge ce);
        if (master_din) begin
            master_rdata <= 16'hdead;
            $display("Master read from %x",master_address);
        end else begin
            $display("Master write to %x: %x", master_address, master_wdata);
        end
        master_rply  <= 1'b1;
    end
    
    reg [3:0] emu_state;
    
    localparam EMU_STATE_ADDR_LO  = 4'h1;
    localparam EMU_STATE_ADDR_HI  = 4'h2;
    localparam EMU_STATE_TAG      = 4'h3;
    localparam EMU_STATE_WDATA_LO = 4'h4;
    localparam EMU_STATE_WDATA_HI = 4'h5;
    localparam EMU_STATE_RDATA    = 4'h6;
    
    initial emu_state = EMU_STATE_ADDR_LO;

    reg [15:0] disk_address_register;
    
    wire [3:0] sector_address    = disk_address_register[3:0]  ;  //
    wire surface           = disk_address_register[4]    ;  //
    wire [7:0] cylinder_address  = disk_address_register[12:5] ;  //
    wire [2:0] drive_select      = disk_address_register[15:13];  //
    
    integer wc;
    
    always @(posedge tx_valid) begin
        tx_busy <= 1'b1;
        
        case (emu_state)
            EMU_STATE_ADDR_LO : begin
                $display("ADDR LO: %x", tx_data);
                disk_address_register[7:0] <= tx_data;
                emu_state <= EMU_STATE_ADDR_HI;
            end
            EMU_STATE_ADDR_HI : begin
                $display("ADDR HI: %x", tx_data);
                disk_address_register[15:8] <= tx_data;
                emu_state <= EMU_STATE_TAG;
                wc        <= 0;
            end
            EMU_STATE_TAG : begin
                case (tx_data)
                    8'h04: begin
                        $display("End of transmission");
                        emu_state <= EMU_STATE_ADDR_LO;
                    end
                    8'h05: begin
                        $display("Read next word (DISK %d, CYLINDER %d, SURFACE %d, SECTOR %d, WORD %d)", drive_select, cylinder_address, surface, sector_address, wc);
                        wc <= wc+1;
                        emu_state <= EMU_STATE_TAG;
                    end
                    8'h0F: begin
                        $display("Write next word (DISK %d, CYLINDER %d, SURFACE %d, SECTOR %d, WORD %d)", drive_select, cylinder_address, surface, sector_address, wc);
                        wc <= wc+1;
                        emu_state <= EMU_STATE_WDATA_LO;
                    end
                    default : begin
                        $display("Unknown TAG");
                        $stop;
                    end
                endcase
            end
            EMU_STATE_WDATA_LO : begin
                $display("WDATA LO: %x",tx_data);
                emu_state <= EMU_STATE_WDATA_HI;
            end
            EMU_STATE_WDATA_HI : begin
                $display("WDATA HI: %x",tx_data);
                emu_state <= EMU_STATE_TAG;
            end
        endcase
        repeat (10) @(ce);
        tx_busy <= 1'b0;
    end
    
    always @(posedge tx_valid) begin
        if (tx_data == 8'h5) begin
            rx_data <= rx_data + 1;
            repeat (25) @(ce);
            rx_valid <= 1'b1;
            @(posedge clock);
            rx_valid <= 1'b0;
            rx_data <= rx_data + 1;
            repeat (25) @(ce);
            rx_valid <= 1'b1;
            @(posedge clock);
            rx_valid <= 1'b0;
        end
    end
    
    initial begin
        $dumpfile("sim.vcd");
        $dumpvars();
    
        clock <= 1'b0;
        reset <= 1'b1;
        ce_count <= 4'b0;
        
        // -- qbus slave
        slave_write <= 1'b0; //     reg         
        slave_read <= 1'b0; //     reg         
        slave_write_byte <= 1'b0; //     reg         
        slave_din <= 1'b0; //     reg         
        slave_dout <= 1'b0; //     reg         
        slave_address <= 1'b0; //     reg [15:0]  
        slave_wdata <= 1'b0; //     reg [15:0]  

        // -- qbus master
        master_dmgo <= 1'b0; //     reg         
        master_rdata <= 1'b0; //     reg [15:0]  
        master_ar <= 1'b0; //     reg         
        master_rply <= 1'b0; //     reg         
            
        // -- interrupt
        iako <= 1'b0; //     reg         

        // -- Serial I/O
        tx_busy <= 1'b0; //     reg           
        rx_data <= 1'b0; //     reg  [7:0]   
        rx_valid <= 1'b0; //     reg          
        
        repeat (5) @(posedge clock);
        reset <= 1'b0;
        repeat (5) @(posedge clock);
        // Set up bus address
        WRITE_SLAVE(ADDRESS_RKBA,16'o1000);
        // Set up drive address
        WRITE_SLAVE(ADDRESS_RKDA,{3'b101,8'h10,1'b0,4'b1100});
        // Set up word count
        WRITE_SLAVE(ADDRESS_RKWC,~16'h100+16'b1);
        // Start read op
        WRITE_SLAVE(ADDRESS_RKCS,{1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,2'b0,3'b010,1'b1});
        @(posedge irq);
        WRITE_SLAVE(ADDRESS_RKCS,16'b0);
        repeat (1000) @(posedge clock);
        // Set up bus address
        WRITE_SLAVE(ADDRESS_RKBA,16'o1000);
        // Set up drive address
        WRITE_SLAVE(ADDRESS_RKDA,{3'b101,8'h10,1'b0,4'b1100});
        // Set up word count
        WRITE_SLAVE(ADDRESS_RKWC,~16'h100+16'b1);
        // Start write op
        WRITE_SLAVE(ADDRESS_RKCS,{1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,2'b0,3'b001,1'b1});
        @(posedge irq);
        WRITE_SLAVE(ADDRESS_RKCS,16'b0);
        repeat (1000) @(posedge clock);
        // Set up bus address
        WRITE_SLAVE(ADDRESS_RKBA,16'o1000);
        // Set up drive address
        WRITE_SLAVE(ADDRESS_RKDA,{3'b101,8'h10,1'b1,4'b1100});
        // Set up word count
        WRITE_SLAVE(ADDRESS_RKWC,~16'h10+16'b1);
        // Start write op
        WRITE_SLAVE(ADDRESS_RKCS,{1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,2'b0,3'b001,1'b1});
        @(posedge irq);
        WRITE_SLAVE(ADDRESS_RKCS,16'b0);
        repeat (1000) @(posedge clock);
        $finish();
    end
    
    
    

endmodule
