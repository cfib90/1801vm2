
// synopsys translate_off
`timescale 1 ps / 1 ps
// synopsys translate_on
module vm1_aram (
	address_a,
	address_b,
	byteena_a,
	clock,
	data_a,
	data_b,
	wren_a,
	wren_b,
	q_a,
	q_b);

	input	[5:0]  address_a;
	input	[5:0]  address_b;
	input	[1:0]  byteena_a;
	input	  clock;
	input	[15:0]  data_a;
	input	[15:0]  data_b;
	input	  wren_a;
	input	  wren_b;
	output	[15:0]  q_a;
	output	[15:0]  q_b;
endmodule
