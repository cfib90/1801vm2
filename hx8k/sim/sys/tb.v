`timescale 1 ps / 1 ps
`default_nettype none
module tb();

    reg clock;
    reg [8:0] ce_cntr;
    reg reset;

    wire sys_clk;
    wire sys_reset;
    wire sys_ce;
    wire sys_ce_clci;
    reg sys_ce_50hz;
    // -- 1801vm2 bidirs
    wire [15:0] sys_ad_i;
    wire  [15:0] sys_ad_o;
    wire  sys_ad_oe;
    // -- 1801vm2 s
    wire sys_sp1;
    wire sys_dmgo;
    wire sys_dout;
    wire sys_wtbt;
    wire sys_sync;
    wire sys_din;
    wire sys_iako;
    wire sys_init;
    wire sys_sel;
    wire sys_pwr_5v_good;
    // -- 1801vm2 s
    wire sys_sp2;
    wire sys_dmr;
    wire sys_sack;
    wire sys_rply;
    wire sys_ar;
    wire sys_aclo;
    wire sys_dclo;
    wire sys_virq;
    wire sys_halt;
    wire sys_evnt;
    // -- serial
    wire sys_rx;
    wire sys_tx;
    // -- SPIMEM

    wire [3:0] sys_sio_i;
    wire [3:0] sys_sio_o;
    wire [3:0] sys_sio_oen;
    wire sys_CS_N;
    wire sys_SCK;

    // -- Audio
    wire sys_pwm;
    // -- dbg
    wire [7:0] dbg;
    
    // -- Wires for the 1801vm1 simulation model
    wire         k1801vm1_clk;
    wire         k1801vm1_ce_p;
    wire         k1801vm1_ce_n;
    wire         k1801vm1_ce_timer;
     
    wire [1:0]   k1801vm1_pa;        	// processor number
    wire         k1801vm1_init_in;   	// peripheral reset input
    wire         k1801vm1_init_out;  	// peripheral reset output
                            	//
    wire         k1801vm1_dclo;      	// processor reset
    wire         k1801vm1_aclo;      	// power fail notoficaton
    wire [3:1]   k1801vm1_irq;       	// radial interrupt requests
    wire         k1801vm1_virq;      	// vectored interrupt request
                            	//
    wire [15:0]  k1801vm1_ad_in;     	// data bus input
    wire [15:0]  k1801vm1_ad_out;    	// address/data bus output
    wire         k1801vm1_ad_ena;    	// address/data bus enable
                            	//
    wire         k1801vm1_dout_in;   	// data output strobe input
    wire         k1801vm1_dout_out;  	// data output strobe output
    wire         k1801vm1_din_in;    	// data input strobe input
    wire         k1801vm1_din_out;   	// data input strobe output
    wire         k1801vm1_wtbt;      	// write/byte status
    wire         k1801vm1_ctrl_ena;  	// enable control outputs
    wire         k1801vm1_rmw;        	// read-modify-write
                            	//
    wire         k1801vm1_sync_in;   	// address strobe input
    wire         k1801vm1_sync_out;  	// address strobe output
    wire         k1801vm1_sync_ena;  	// address strobe enable
                            	//
    wire         k1801vm1_rply_in;   	// transaction reply input
    wire         k1801vm1_rply_out;  	// transaction reply output
                            	//
    wire         k1801vm1_dmr_in;    	// bus request shared line
    wire         k1801vm1_dmr_out;   	//
                            	//
    wire         k1801vm1_sack_in;   	// bus acknowlegement
    wire         k1801vm1_sack_out;  	// bus acknowlegement
                            	//
    wire         k1801vm1_dmgi;      	// bus granted input
    wire         k1801vm1_dmgo;      	// bus granted output
    wire         k1801vm1_iako;      	// interrupt vector input
    wire         k1801vm1_sp;        	// peripheral timer input
    wire [2:1]   k1801vm1_sel;       	// register select outputs
    wire         k1801vm1_bsy;        	// bus busy flag
   
    wire [3:0] SIO_SPI;
    wire sys_ce_1801_clco;
    wire [7:0] sys_led;
    
    assign sys_sio_i = SIO_SPI;
    assign SIO_SPI[0] = sys_sio_oen[0] ? sys_sio_o[0] : 1'bz;
    assign SIO_SPI[1] = sys_sio_oen[1] ? sys_sio_o[1] : 1'bz;
    assign SIO_SPI[2] = sys_sio_oen[2] ? sys_sio_o[2] : 1'bz;
    assign SIO_SPI[3] = sys_sio_oen[3] ? sys_sio_o[3] : 1'bz;
    
    M23LC1024 spimem (.SI_SIO0(SIO_SPI[0]), .SO_SIO1(SIO_SPI[1]), .SCK(sys_SCK), .CS_N(sys_CS_N), .SIO2(SIO_SPI[2]), .HOLD_N_SIO3(SIO_SPI[3]), .RESET(sys_reset));

    sys1801vm2 system (
        .clk         (sys_clk),
        .reset       (sys_reset),
        .ce          (sys_ce_1801_clco),
        .ce_clci     (sys_ce_clci),
        .ce_50hz     (sys_ce_50hz),
        // -- 1801vm2 bidirs
        .ad_i        (sys_ad_i),
        .ad_o        (sys_ad_o),
        .ad_oe       (sys_ad_oe),
        // -- 1801vm2 inputs
        .sp1         (sys_sp1),
        .dmgo        (sys_dmgo),
        .dout        (sys_dout),
        .wtbt        (sys_wtbt),
        .sync        (sys_sync),
        .din         (sys_din),
        .iako        (sys_iako),
        .init        (sys_init),
        .sel         (sys_sel),
        .pwr_5v_good (sys_pwr_5v_good),
        // -- 1801vm2 outputs
        .sp2         (sys_sp2),
        .dmr         (sys_dmr),
        .sack        (sys_sack),
        .rply        (sys_rply),
        .ar          (sys_ar),
        .aclo        (sys_aclo),
        .dclo        (sys_dclo),
        .virq        (sys_virq),
        .halt        (sys_halt),
        .evnt        (sys_evnt),
        // -- serial
        .tx          (sys_tx),
        .rx          (sys_rx),
        // -- SPI
        .sio_i(sys_sio_i),
        .sio_o(sys_sio_o),
        .sio_oen(sys_sio_oen),
        .cs_n(sys_CS_N),
        .sck(sys_SCK),
        // -- audio
        .pwm(sys_pwm),
        // -- dbg
        .dbg    (sys_led)
    );

    // 1801vm1 simualtion model
    
    vm1_qbus_se CPU (
    .pin_clk      (k1801vm1_clk      ),
    .pin_ce_p     (k1801vm1_ce_p     ),
    .pin_ce_n     (k1801vm1_ce_n     ),
    .pin_ce_timer (k1801vm1_ce_timer ),

    .pin_pa       (k1801vm1_pa       ),
    .pin_init_in  (k1801vm1_init_in  ),
    .pin_init_out (k1801vm1_init_out ),

    .pin_dclo     (k1801vm1_dclo     ),
    .pin_aclo     (k1801vm1_aclo     ),
    .pin_irq      (k1801vm1_irq      ),
    .pin_virq     (k1801vm1_virq     ),

    .pin_ad_in    (k1801vm1_ad_in    ),
    .pin_ad_out   (k1801vm1_ad_out   ),
    .pin_ad_ena   (k1801vm1_ad_ena   ),

    .pin_dout_in  (k1801vm1_dout_in  ),
    .pin_dout_out (k1801vm1_dout_out ),
    .pin_din_in   (k1801vm1_din_in   ),
    .pin_din_out  (k1801vm1_din_out  ),
    .pin_wtbt     (k1801vm1_wtbt     ),
    .pin_ctrl_ena (k1801vm1_ctrl_ena ),
    .pin_rmw      (k1801vm1_rmw      ),

    .pin_sync_in  (k1801vm1_sync_in  ),
    .pin_sync_out (k1801vm1_sync_out ),
    .pin_sync_ena (k1801vm1_sync_ena ),

    .pin_ar_in(sys_ar),
    .pin_rply_in  (k1801vm1_rply_in  ),
    .pin_rply_out (k1801vm1_rply_out ),

    .pin_dmr_in   (k1801vm1_dmr_in   ),
    .pin_dmr_out  (k1801vm1_dmr_out  ),

    .pin_sack_in  (k1801vm1_sack_in  ),
    .pin_sack_out (k1801vm1_sack_out ),

    .pin_dmgi     (k1801vm1_dmgi     ),
    .pin_dmgo     (k1801vm1_dmgo     ),
    .pin_iako     (k1801vm1_iako     ),
    .pin_sp       (k1801vm1_sp       ),
    .pin_sel      (k1801vm1_sel      ),
    .pin_bsy      (k1801vm1_bsy)     );



   assign k1801vm1_pa = 2'b0;
   assign k1801vm1_ad_in  = sys_ad_o;
   assign k1801vm1_din_in = 1'b0;
   assign k1801vm1_dout_in = 1'b0;
   assign k1801vm1_dclo = sys_dclo;
   assign k1801vm1_aclo = sys_aclo;
   assign k1801vm1_virq = sys_virq;
   assign sys_dmgo = k1801vm1_dmgo;
   assign k1801vm1_dmgi = 1'b0;
   assign k1801vm1_sack_in = sys_sack;
   assign sys_init = k1801vm1_init_out;
   assign sys_din = k1801vm1_din_out;
   assign sys_dout = k1801vm1_dout_out;
   assign sys_wtbt = k1801vm1_wtbt;
   assign k1801vm1_irq = 3'b0;
   assign k1801vm1_sp = 1'b0;
   assign sys_iako = k1801vm1_iako;
   assign sys_ad_i = k1801vm1_ad_out;
   assign k1801vm1_dmr_in = sys_dmr;
   assign sys_pwr_5v_good = 1'b1;
   assign sys_reset = reset;
   assign k1801vm1_sync_in = 1'b0;
   assign k1801vm1_init_in = 1'b0;
   assign k1801vm1_rply_in = sys_rply;
   assign k1801vm1_sync_in = 1'b0;
   
    always
        #1 clock <= ~clock;
   
   initial begin
       $dumpfile("sim.vcd");
       $dumpvars;
       $stop;
       clock   <= 1'b0;
       reset   <= 1'b1;
       ce_cntr <= 8'b0;
       repeat(5) @(posedge clock);
       reset <= 1'b0;
       @(negedge sys_dclo);
       while (1) begin
           repeat(1000000) @(posedge clock);
           $stop;
       end
   end
   
   always @(posedge clock) begin
       ce_cntr <= (ce_cntr[8]) ? 8'b0 : ce_cntr+1;
       
   end
       
       
    always begin
        sys_ce_50hz <= 1'b0;
        repeat (10000) @(negedge clock);
        sys_ce_50hz <= 1'b1;
        repeat (1) @(negedge clock);
    end
       
       
   assign sys_clk = clock;
   assign k1801vm1_clk = clock;
   assign k1801vm1_ce_timer = ce_cntr[8];

   wire ce_clci;
   reg clci;
   reg [15:0] clci_cnt;
   reg clci_dly;
   assign ce_clci = ~clci_dly & clci;
    
   reg ce_clco;
   reg [3:0] ce_dly;
    
    // Generate 40kHz clci for 20kHz clco
    always @(posedge clock) begin
        if (reset) begin
            clci_cnt <= 16'd60;
            clci     <= 1'b0;
            clci_dly <= 1'b0;
            ce_clco  <= 1'b0;
        end else begin
            clci_dly <= clci;
            clci_cnt <= clci_cnt - 1;
            if (clci_cnt == 16'd0) begin
                clci <= ~clci;
                clci_cnt <= 16'd60;
            end
            if (ce_clci) begin
                ce_clco <= ~ce_clco;
            end
            ce_dly <= {ce_dly[2:0],ce_clci};
        end
    end

   assign sys_sel = 1'b0;
   assign k1801vm1_ce_p = ce_clci;
   assign k1801vm1_ce_n = ce_dly[2];
   assign sys_ce      = ce_clco;
   assign sys_ce_clci =  ce_clci;
   assign sys_ce_1801_clco = ce_clco;
   assign sys_sync = k1801vm1_sync_out;


   uart_pty #(115200,24000000) uart (clock,sys_tx,sys_rx);

endmodule
