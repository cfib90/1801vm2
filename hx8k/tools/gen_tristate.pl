my $head =<<"END_HEAD";
`default_nettype none
module ad_tristate (input  wire clk,
                    input  wire [15:0] ad_o,
                    output wire [15:0] ad_i,
                    inout  wire [15:0] ad_n,
                    input  wire ad_oe);

    wire [15:0] ad_i_n;
    assign ad_i = ~ad_i_n;

END_HEAD

print $head;

for (my $i = 0; $i < 16; $i++) {
    
    my $text=<<"END_TRISTATE";
    
    // -- PIN ad[$i] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_$i (
        .PACKAGE_PIN(ad_n[$i]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[$i]),
        .D_IN_0(ad_i_n[$i])
    );
    
END_TRISTATE

    print $text;
}


print "endmodule"
