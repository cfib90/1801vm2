// Just used for linting

module pll(
	input  clock_in,
	output clock_out,
	output locked
	);

   assign clock_out = clock_in;
   assign locked = 1'b1;

endmodule

module ad_tristate (input  wire clk,
                    input  wire [15:0] ad_o,
                    output wire [15:0] ad_i,
                    inout  wire [15:0] ad_n,
                    input  wire ad_oe);

    wire [15:0] ad_i_n;
    assign ad_i = ~ad_i_n;

    genvar i;
    generate
        for (i=0; i<16; i=i+1)
            assign ad_n[i] = (ad_oe) ? ~ad_o[i] : 1'bz;
    endgenerate

endmodule


module SB_IO #(parameter PIN_TYPE = 0,
               parameter PULLUP = 0)
               
            (inout wire PACKAGE_PIN,
             input wire OUTPUT_ENABLE,
             input wire D_OUT_0,
             output wire D_IN_0);
          
    assign PACKAGE_PIN = (OUTPUT_ENABLE == 1'b1) ? D_OUT_0 : 1'bz;
    assign D_IN_0 = PACKAGE_PIN;
             
endmodule

