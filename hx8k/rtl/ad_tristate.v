`default_nettype none
module ad_tristate (input  wire clk,
                    input  wire [15:0] ad_o,
                    output wire [15:0] ad_i,
                    inout  wire [15:0] ad_n,
                    input  wire ad_oe);

    wire [15:0] ad_i_n;
    assign ad_i = ~ad_i_n;

    
    // -- PIN ad[0] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_0 (
        .PACKAGE_PIN(ad_n[0]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[0]),
        .D_IN_0(ad_i_n[0])
    );
    
    
    // -- PIN ad[1] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_1 (
        .PACKAGE_PIN(ad_n[1]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[1]),
        .D_IN_0(ad_i_n[1])
    );
    
    
    // -- PIN ad[2] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_2 (
        .PACKAGE_PIN(ad_n[2]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[2]),
        .D_IN_0(ad_i_n[2])
    );
    
    
    // -- PIN ad[3] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_3 (
        .PACKAGE_PIN(ad_n[3]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[3]),
        .D_IN_0(ad_i_n[3])
    );
    
    
    // -- PIN ad[4] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_4 (
        .PACKAGE_PIN(ad_n[4]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[4]),
        .D_IN_0(ad_i_n[4])
    );
    
    
    // -- PIN ad[5] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_5 (
        .PACKAGE_PIN(ad_n[5]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[5]),
        .D_IN_0(ad_i_n[5])
    );
    
    
    // -- PIN ad[6] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_6 (
        .PACKAGE_PIN(ad_n[6]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[6]),
        .D_IN_0(ad_i_n[6])
    );
    
    
    // -- PIN ad[7] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_7 (
        .PACKAGE_PIN(ad_n[7]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[7]),
        .D_IN_0(ad_i_n[7])
    );
    
    
    // -- PIN ad[8] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_8 (
        .PACKAGE_PIN(ad_n[8]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[8]),
        .D_IN_0(ad_i_n[8])
    );
    
    
    // -- PIN ad[9] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_9 (
        .PACKAGE_PIN(ad_n[9]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[9]),
        .D_IN_0(ad_i_n[9])
    );
    
    
    // -- PIN ad[10] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_10 (
        .PACKAGE_PIN(ad_n[10]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[10]),
        .D_IN_0(ad_i_n[10])
    );
    
    
    // -- PIN ad[11] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_11 (
        .PACKAGE_PIN(ad_n[11]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[11]),
        .D_IN_0(ad_i_n[11])
    );
    
    
    // -- PIN ad[12] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_12 (
        .PACKAGE_PIN(ad_n[12]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[12]),
        .D_IN_0(ad_i_n[12])
    );
    
    
    // -- PIN ad[13] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_13 (
        .PACKAGE_PIN(ad_n[13]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[13]),
        .D_IN_0(ad_i_n[13])
    );
    
    
    // -- PIN ad[14] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_14 (
        .PACKAGE_PIN(ad_n[14]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[14]),
        .D_IN_0(ad_i_n[14])
    );
    
    
    // -- PIN ad[15] ---------------------------------------------------
    
    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
        .PULLUP(1'b 0)
    ) ad_15 (
        .PACKAGE_PIN(ad_n[15]),
        .OUTPUT_ENABLE(ad_oe),
        .D_OUT_0(~ad_o[15]),
        .D_IN_0(ad_i_n[15])
    );
    
endmodule