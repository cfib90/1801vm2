`default_nettype none

module uart_mux_top #(parameter NUM_PORTS = 2) (input wire clock,
                                                input wire reset,
                                                
                                                // -- Interface to clients
                                                input wire  [NUM_PORTS*8-1:0]  txdata,
                                                input wire  [NUM_PORTS-1:0]    tx_valid,
                                                output reg  [NUM_PORTS-1:0]    tx_busy,
                                                output reg  [NUM_PORTS*8-1:0]  rxdata,
                                                output reg  [NUM_PORTS-1:0]    rx_valid,
                                                
                                                // -- Interface to actual UART
                                                output reg [7:0] uart_txdata,
                                                output reg  uart_tx_valid,
                                                input  wire uart_tx_busy,
                                                input wire [7:0] uart_rxdata,
                                                input wire uart_rx_valid
                                                );
                                                
                                                
    reg [NUM_PORTS-1:0] tx_request;
    reg [7:0]           tx_buffer [NUM_PORTS-1:0];
    reg [7:0]           tx_state;
    reg uart_tx_busy_reg;
    reg prefix_sent;
    integer i;
    reg wait_done;
    
    // Multiplex TX
    
    always @(posedge clock) begin
        if (reset) begin
            tx_request  <= {NUM_PORTS{1'b0}};
            tx_busy     <= 1'b0;
            tx_state    <= {8'b0};
            prefix_sent <= 1'b0;
            uart_tx_valid <= 1'b0;
            uart_tx_busy_reg <= 1'b1;
            wait_done        <= 1'b0;
        end else begin
            uart_tx_valid <= 1'b0;
            uart_tx_busy_reg <= uart_tx_busy;
            for (i = 0; i < NUM_PORTS; i=i+1) begin
                tx_request[i] <= tx_request[i] | tx_valid[i];
                if (tx_valid[i]) begin
                    $display("UART: CHANNEL %d: %c (%x)",i,txdata[i*8+:8],txdata[i*8+:8]);
                    tx_buffer[i] <= txdata[i*8+:8];
                    tx_busy[i]   <= 1'b1;
                end
            end
            if (~wait_done) begin
                if (tx_request[tx_state]) begin
                    if (prefix_sent) begin
                        prefix_sent          <= 1'b0;
                        tx_request[tx_state] <= 1'b0;
                        uart_txdata          <= tx_buffer[tx_state];
                        tx_state             <= (tx_state==(NUM_PORTS-1)) ? 0 : tx_state+1;
                        tx_busy[tx_state]    <= 1'b0;
                    end else begin
                        prefix_sent          <= 1'b1;
                        uart_txdata          <= tx_state;
                    end
                    uart_tx_valid <= 1'b1;
                    wait_done     <= 1'b1;
                end else begin
                    tx_state <= (tx_state==(NUM_PORTS-1)) ? 0 : tx_state+1;
                end
            end else begin
                wait_done <= ~(~uart_tx_busy & uart_tx_busy_reg);
            end
        end
    end
    
    // Multiplex RX
    reg prefix_received;
    reg [7:0] prefix;
    
    always @(posedge clock) begin
        if (reset) begin
            prefix_received <= 1'b0;
            prefix          <= 1'b0;
            rxdata          <= {NUM_PORTS*8{1'b0}};
            rx_valid        <= {NUM_PORTS{1'b0}};
        end else begin
            rx_valid        <= {NUM_PORTS{1'b0}};
            if (uart_rx_valid) begin
                if (prefix_received) begin
                    prefix_received      <= 1'b0;
                    rxdata[prefix*8+:8]  <= uart_rxdata;
                    rx_valid[prefix]     <= 1'b1;
                end else begin
                    prefix_received      <= 1'b1;
                    prefix               <= uart_rxdata;
                end
            end
        end
    end


endmodule
