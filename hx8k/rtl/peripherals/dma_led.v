module dma_led #(parameter LED_ADDR = 16'hFEEC)
                (input wire clock,
                input wire reset,
                input wire ce,
                input wire ce_50hz,
                output reg dmr,
                input wire dmgo,
                output reg sack,
                output reg [15:0] writedata,
                input wire [15:0] readdata,
                output reg dout,
                output reg din,
                output reg wtbt,
                output reg sync,
                input wire ar,
                input wire rply);
                
    reg [8:0] wdt;
    reg [4:0] cntr;
    reg [7:0] dmacntr;
    
    always @(posedge clock) begin
        if (reset)
            cntr <= 5'b0;
        else begin
            if (ce_50hz) begin
                cntr <= cntr + 1;
                if (cntr[4])
                    cntr <= 5'b0;
            end
        end
    end
    
    reg [7:0] state;
    
    localparam IDLE       = 8'b00000001,
               WAIT_DMG   = 8'b00000010,
               XFER_START = 8'b00000100,
               XFER_ADDR  = 8'b00001000,
               WAIT_AR    = 8'b00010000,
               XFER_WDATA = 8'b00100000,
               XFER_DOUT  = 8'b01000000,
               WAIT_RPLY  = 8'b10000000;
    
    always @(posedge clock) begin
        if (reset) begin
            state <= IDLE;
            dmr  <= 1'b0;
            sync <= 1'b0;
            sack <= 1'b0;
            din  <= 1'b0;
            dout <= 1'b0;
            wdt  <= 9'b0;
            wtbt <= 1'b0;
            dmacntr <= 8'b0;
            writedata <= 16'b0;
        end else if (ce) begin
            sync <= 1'b0;
            dout <= 1'b0;
            dmr  <= dmr & ~dmgo;
            
            case (state)
                IDLE: begin
                    if (cntr[4]) begin
                        dmacntr <= dmacntr+1;
                        dmr   <= 1'b1;
                        state <= WAIT_DMG;
                        wdt   <= 9'b011111111;
                    end
                end
                WAIT_DMG : begin
                    wdt <= wdt-1;
                    if (wdt[8]) begin
                        dmr   <= 1'b0;
                        state <= IDLE;
                    end
                    if (dmgo) begin
                        state <= XFER_START;
                    end
                end
                XFER_START : begin
                    sack      <= 1'b1;
                    state     <= XFER_ADDR;
                end
                XFER_ADDR : begin
                    writedata <= LED_ADDR;
                    sync      <= 1'b1;
                    state     <= WAIT_AR;
                    wdt       <= 9'b011111111;
                end
                WAIT_AR : begin
                    wdt <= wdt-1;
                    if (wdt[8]) begin
                        dmr   <= 1'b0;
                        sack  <= 1'b0;
                        state <= IDLE;
                    end
                    if (ar) begin
                        state <= XFER_WDATA;
                    end
                end
                XFER_WDATA : begin
                    writedata <= {8'b0,dmacntr};
                    state     <= XFER_DOUT;
                end
                XFER_DOUT : begin
                    dout      <= 1'b1;
                    state     <= WAIT_RPLY;
                    wdt       <= 9'b011111111;
                end
                WAIT_RPLY : begin
                    wdt <= wdt-1;
                    if (wdt[8] || rply) begin
                        state <= IDLE;
                        sack  <= 1'b0;
                    end
                end
            endcase
        end
    end
    
endmodule
