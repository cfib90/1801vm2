`default_nettype none
module ODT #(
    parameter BASE_ADDRESS    = 16'o173000,  // -- ODT is always mapped to this address
    parameter TOP_ADDRESS     = 16'o174777,  // -- Is 512 Words (1024B)
    parameter INIT_FILE       = "odt.mem"
   
   // 15 14 13 12 11 10 09
   // 1  1  1  1  0  1  1
   // 1  1  1  1  1  0  0 
) (
    input  wire clk,
    input  wire reset,
    // -- bus
    input  wire         read,
    input  wire         write,
    input  wire         din,
    input  wire         dout,
    input  wire [15:0]  address,
    output reg  [15:0]  rdata,
    output reg          read_rply,
    output reg          write_rply,
    output wire         addr_match
);

    wire odt_addr_match;
    assign addr_match = odt_addr_match;
    assign odt_addr_match = (address[15:9] == BASE_ADDRESS[15:9] || 
                             address[15:9] == TOP_ADDRESS[15:9]);
    reg [15:0] odt_rom [511:0];

    initial
        $readmemh(INIT_FILE,odt_rom);
    
    always @(posedge clk) begin
        if (reset) begin
            read_rply                        <= 1'b0;
            write_rply                       <= 1'b0;
            rdata                            <= 16'b0;
        end else begin
            read_rply                        <= read_rply  & din;
            write_rply                       <= write_rply & dout;
            if (odt_addr_match & read) begin
                read_rply                            <= 1'b1;
                rdata                                <= odt_rom[address[8:1]];
            end
            if (odt_addr_match & write) begin
                write_rply                   <= 1'b1;
            end
        end
    end



endmodule
