`default_nettype none
module kw11 #(parameter ADDRESS = 16'o177546, parameter VECTOR = 16'o000100)
            (input wire clock,
             input wire reset,
             input wire ce50hz,
             input wire write,
             input wire read,
             input wire din,
             input wire dout,
             input wire [15:0] address,
             output wire [15:0] rdata,
             output wire addr_match,
             input wire [15:0] wdata,
             output reg rply,
             output wire irq,
             input wire iako,
             output reg iako_rply,
             output wire [15:0] interrupt_vector);
             
    wire addr_match_comb;
    assign addr_match_comb = (address[15:0] == ADDRESS[15:0]);
    assign addr_match      = addr_match_comb;
    assign interrupt_vector = VECTOR;
             
    reg interrupt_enable;
    reg reg50hz;
    assign rdata    = {8'b0,reg50hz,interrupt_enable,6'b0};
    assign irq      = reg50hz & interrupt_enable;
    
    always @(posedge clock) begin
        if (reset) begin
            rply <= 1'b0;
            reg50hz <= 1'b0;
            interrupt_enable <= 1'b0;
        end else begin
            rply <= rply & (din | dout);
            if(addr_match_comb & write) begin
                rply <= 1'b1;
                //interrupt_enable <= wdata[6];
                reg50hz          <= wdata[7];
            end
            if(addr_match_comb & read) begin
                rply <= 1'b1;
                interrupt_enable <= 1'b0;
            end
            if (ce50hz) begin
                reg50hz <= 1'b1;
            end
        end
    end
             
    // -- Interrupt rply process
    always @(posedge clock) begin
        if (reset) begin
            iako_rply      <= 1'b0;
        end else begin
            iako_rply      <= iako_rply & iako;
            if (iako) begin
                iako_rply <= 1'b1;
            end
        end
    end
    
endmodule
