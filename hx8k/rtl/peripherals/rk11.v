`default_nettype none
module rk11 #(
    parameter BASE_ADDRESS    = 16'o177400,  // -- Default
    parameter DONE_INT_VECTOR = 16'o000220   // -- Default
) (
    input  wire clock,
    input  wire reset,
    input  wire ce,
    // -- qbus slave
    input  wire         slave_write,
    input  wire         slave_read,
    input  wire         slave_write_byte,
    input  wire         slave_din,
    input  wire         slave_dout,
    input  wire [15:0]  slave_address,
    input  wire [15:0]  slave_wdata,
    output reg  [15:0]  slave_rdata,
    output wire         slave_rply,
    output wire         slave_addr_match,
    
    // -- qbus master
     output reg         master_dmr,
     input wire         master_dmgo,
     output reg         master_sack,
     output reg [15:0]  master_ad,
     input wire [15:0]  master_rdata,
     output reg         master_dout,
     output reg         master_din,
     output reg         master_wtbt,
     output reg         master_sync,
     input wire         master_ar,
     input wire         master_rply,
    
    // -- interrupt
    output reg          irq,
    output wire [15:0]  irq_vector,
    output reg          iako_rply,
    input  wire         iako,
    
    // -- Serial I/O
    output reg  [7:0]   tx_data,
    output reg          tx_valid,
    input  wire         tx_busy,
    
    input wire  [7:0]   rx_data,
    input wire          rx_valid,
    
    output wire [7:0]   debug
);

    wire ctrl_reset;
    wire drive_reset;
    
    reg  [3:0] sw_rst;
    reg  [3:0] dr_rst;
    
    assign ctrl_reset = reset; // sw_rst[3];
    assign drive_reset = reset; // dr_rst[3];
    
    initial begin
        master_wtbt <= 1'b0;
    end
    
    assign irq_vector = DONE_INT_VECTOR;

    // -----------------------------------------------------------------
    // --Register interface
    // -----------------------------------------------------------------
    
    // -- Register +0 : DRIVE STATUS REGISTER (RKDS)
    localparam ADDRESS_OFFSET_RKDS   = 4'o0;
    
    wire [3:0] sector_counter;                       // 0-3
    reg sector_counter_equals_sector_address;       // 4
    reg write_protect_status;                       // 5
    wire access_ready;                               // 6   (ARDY)
    reg drive_ready;                                // 7   (DRY)
    reg sector_counter_OK;                          // 8
    reg seek_incomplete;                            // 9
    reg drive_unsafe;                               // 10  (DRU)
    reg hi_density_disk_drive;                      // 11  (HDEN)
    reg drive_power_low;                            // 12
    wire [2:0] identification_of_drive;              // 13-15
    
    assign access_ready   = control_ready;
    assign sector_counter = disk_address_register[3:0];
    assign identification_of_drive = disk_address_register[15:13];
    
    // Always set
    initial begin
        drive_ready                             <= 1'b1; // These conditions always apply: 
                                                         // a. properly supplied with power
                                                         // b. loaded with a disk cartridge
                                                         // c. the disk drive door is closed
                                                         // d. the LOAD/RUN switch is in the RUN position
                                                         // e. the disk is rotating at the proper speed
                                                         // f. the heads are properly loaded
                                                         // g. the disk is not in a DRU condition
        sector_counter_equals_sector_address    <= 1'b1;
        drive_unsafe                            <= 1'b0; 
        hi_density_disk_drive                   <= 1'b1; // Only RK05 emulation
        drive_power_low                         <= 1'b0;
    end
    
    wire [15:0] drive_status_register;

    assign drive_status_register[3:0]   = sector_counter;                       // 0-3
    assign drive_status_register[4]     = sector_counter_equals_sector_address;       // 4
    assign drive_status_register[5]     = write_protect_status;                       // 5
    assign drive_status_register[6]     = access_ready;                               // 6   (ARDY)
    assign drive_status_register[7]     = drive_ready;                                // 7   (DRY)
    assign drive_status_register[8]     = sector_counter_OK;                          // 8
    assign drive_status_register[9]     = seek_incomplete;                            // 9
    assign drive_status_register[10]    = drive_unsafe;                               // 10  (DRU)
    assign drive_status_register[11]    = hi_density_disk_drive;                      // 11  (HDEN)
    assign drive_status_register[12]    = drive_power_low;                            // 12
    assign drive_status_register[15:13] = identification_of_drive;              // 13-15

    initial begin
        write_protect_status <= 1'b0;
    end

    
    // -- Register +2 : ERROR REGISTER (RKER)
    localparam ADDRESS_OFFSET_RKER   = 4'o2;
    
    reg write_check_error;       // 0    --
    reg checksum_error;          // 1    --
    reg [2:0] rker_unused;       // 2-4  --
    reg non_existent_sector;     // 5    --
    reg non_existent_cylinder;   // 6    --
    reg non_existent_disk;       // 7    --
    reg read_timing_error;       // 8    --
    reg data_late;               // 9    --
    reg non_existent_memory;     // 10   --
    reg programming_error;       // 11   --
    reg seek_error;              // 12   --
    reg write_lockout_violation; // 13   --
    reg overrun;                 // 14   --
    reg drive_error;             // 15   --
    
    initial begin

        write_check_error <= 1'b0;
        checksum_error    <= 1'b0;
        read_timing_error <= 1'b0;
        rker_unused       <= 3'b0;
        data_late         <= 1'b0;
        write_lockout_violation <= 1'b0;
        drive_error       <= 1'b0;
        seek_error        <= 1'b0;
        programming_error <= 1'b0;
    end
    
    wire [15:0] error_register;
    
    assign error_register[0]   = write_check_error;       // 0    
    assign error_register[1]   = checksum_error;          // 1    
    assign error_register[4:2] = rker_unused;             // 2-4  
    assign error_register[5]   = non_existent_sector;     // 5    
    assign error_register[6]   = non_existent_cylinder;   // 6    
    assign error_register[7]   = non_existent_disk;       // 7    
    assign error_register[8]   = read_timing_error;       // 8    
    assign error_register[9]   = data_late;               // 9    
    assign error_register[10]  = non_existent_memory;     // 10   
    assign error_register[11]  = programming_error;       // 11   
    assign error_register[12]  = seek_error;              // 12   
    assign error_register[13]  = write_lockout_violation; // 13   
    assign error_register[14]  = overrun;                 // 14   
    assign error_register[15]  = drive_error;             // 15   
    
    // -- Register +4 : CONTROL STATUS REGISTER (RKCS)
    localparam ADDRESS_OFFSET_RKCS   = 4'o4;
    
    reg go;                           // 0     //
    reg [2:0] rk_function;            // 1-3   //
    reg interrupt_on_done_enable;     // 6     //
    reg control_ready;                // 7     //
    reg stop_on_soft_error;           // 8     //
    reg read_write_all;               // 9     //
    reg format;                       // 10    //
    reg inhibit_incrementing_rkba;    // 11    //
    reg maintenance_mode;             // 12    //
    reg search_complete;              // 13    //
    wire hard_error;                   // 14    //
    wire error;                       // 15    //
    
    assign hard_error = 1'b0;
    
    wire [15:0] control_status_register;
    
    assign control_status_register[0]   = go;                           
    assign control_status_register[3:1] = rk_function;            
    assign control_status_register[5:4] = bus_address_register[17:16];       
    assign control_status_register[6]   = interrupt_on_done_enable;     
    assign control_status_register[7]   = control_ready;                
    assign control_status_register[8]   = stop_on_soft_error;           
    assign control_status_register[9]   = read_write_all;               
    assign control_status_register[10]  = format;                       
    assign control_status_register[11]  = inhibit_incrementing_rkba;    
    assign control_status_register[12]  = maintenance_mode;             
    assign control_status_register[13]  = search_complete;              
    assign control_status_register[14]  = hard_error;                   
    assign control_status_register[15]  = error;                        
    
    assign error = | error_register;

    // -- Register +6 : WORD COUNT REGISTER (RKWC)
    localparam ADDRESS_OFFSET_RKWC   = 4'o6;
    
    reg [15:0] word_count_register;

    // -- Register +10 : CURRENT BUS ADDRESS REGISTER (RKBA)
    localparam ADDRESS_OFFSET_RKBA   = 4'o10;
    
    reg [17:0] bus_address_register;

    // -- Register +12 : DISK ADDRESS REGISTER (RKDA)
    localparam ADDRESS_OFFSET_RKDA   = 4'o12;
    
    wire [3:0] sector_address;
    wire surface;
    wire [7:0] cylinder_address;
    wire [2:0] drive_select;
    
    reg [15:0] disk_address_register;
    
    assign sector_address    = disk_address_register[3:0]  ;  //
    assign surface           = disk_address_register[4]    ;  //
    assign cylinder_address  = disk_address_register[12:5] ;  //
    assign drive_select      = disk_address_register[15:13];  //

    // -- Register +14 : MAINTENANCE REGISTER (RKMR)
    localparam ADDRESS_OFFSET_RKMR   = 4'o14;
    
    reg [3:0] maintenance_sector_counter;         // 0-3
    reg maintenance_ready_to_read_write_or_seek;  // 6
    reg maintenance_drive_ready;                  // 7
    reg maintenance_logical_address_interlock;    // 11
    reg maintenance_address_acknowledge;          // 12
    reg maintenance_sector_pulse;                 // 13
    reg maintenance_read_data_pulse;              // 14
    reg maintenance_read_clock_pulse;             // 15
    
    wire [15:0] maintenance_register;
    
    assign maintenance_register[3:0]  = maintenance_sector_counter;         // 0-3 //
    assign maintenance_register[5:4]  = 2'b0;
    assign maintenance_register[6]    = maintenance_ready_to_read_write_or_seek;  // 6   //
    assign maintenance_register[7]    = maintenance_drive_ready;                  // 7   //
    assign maintenance_register[10:8] = 3'b0;
    assign maintenance_register[11]   = maintenance_logical_address_interlock;    // 11  //
    assign maintenance_register[12]   = maintenance_address_acknowledge;          // 12  //
    assign maintenance_register[13]   = maintenance_sector_pulse;                  // 13  //
    assign maintenance_register[14]   = maintenance_read_data_pulse;              // 14  //
    assign maintenance_register[15]   = maintenance_read_clock_pulse;             // 15  //
    
    // -- Register +16 : DATA BUFFER REGISTER (RKDB)
    localparam ADDRESS_OFFSET_RKDB   = 4'o16;
    
    reg [15:0] data_buffer;

    // -- Address match    
    wire address_match_comb;
    assign address_match_comb = slave_address[15:4] == BASE_ADDRESS[15:4];
    assign slave_addr_match = address_match_comb;
    
    // -- Read process
    reg slave_read_rply;    
    assign irq_vector = DONE_INT_VECTOR;
        
    always @(posedge clock) begin
        if (ctrl_reset) begin
            slave_read_rply   <= 1'b0;
            slave_rdata       <= 16'b0;
        end else begin
            slave_read_rply   <= slave_read_rply & slave_din;
            
            if (address_match_comb & slave_read) begin
                slave_read_rply   <= 1'b1;
                case (slave_address[3:0])
                    ADDRESS_OFFSET_RKDS : begin
                        slave_rdata <= drive_status_register;
                    end
                    ADDRESS_OFFSET_RKER : begin
                        slave_rdata <= error_register;
                    end
                    ADDRESS_OFFSET_RKCS : begin
                        slave_rdata <= control_status_register;
                    end
                    ADDRESS_OFFSET_RKWC : begin
                        slave_rdata <= word_count_register;
                    end
                    ADDRESS_OFFSET_RKBA : begin
                        slave_rdata <= bus_address_register[15:0];
                    end
                    ADDRESS_OFFSET_RKDA : begin
                        slave_rdata <= disk_address_register;
                    end
                    ADDRESS_OFFSET_RKMR : begin
                        slave_rdata <= maintenance_register;
                    end
                    ADDRESS_OFFSET_RKDB : begin
                        slave_rdata <= data_buffer;
                    end
                endcase;
            end
        end
    end
    
    // -- Write process
    reg slave_write_rply;
    
    reg [15:0] disk_address_register_write;
    
    reg [15:0] word_count_write;
    reg load_word_count;
    
    reg [15:0] current_bus_address_write;
    reg load_current_bus_address;
    reg load_disk_address_register;
    
    always @(posedge clock) begin
        if (ctrl_reset) begin
            load_disk_address_register            <= 1'b0;
            slave_write_rply                      <= 1'b0;
            go                                    <= 1'b0;
            maintenance_logical_address_interlock <= 1'b0;
            maintenance_sector_pulse              <= 1'b0;
            maintenance_read_data_pulse           <= 1'b0;
            maintenance_read_clock_pulse          <= 1'b0;
            maintenance_address_acknowledge       <= 1'b0;
            maintenance_drive_ready               <= 1'b0;
            maintenance_ready_to_read_write_or_seek <= 1'b0;
            maintenance_sector_counter            <= 4'b0;
            load_current_bus_address              <= 1'b0;
            current_bus_address_write             <= 16'b0;
            disk_address_register_write           <= 16'b0;
            word_count_write                      <= 16'b0;
            load_word_count                       <= 1'b0;
            rk_function                           <= 3'b0;
            read_write_all                        <= 1'b0;
            maintenance_mode                      <= 1'b0;
            stop_on_soft_error                    <= 1'b0;
            seek_incomplete                       <= 1'b0;
            inhibit_incrementing_rkba             <= 1'b0;
        end else begin
            slave_write_rply                      <= slave_write_rply & slave_dout;
            go                                    <= 1'b0;                       // strobe
            maintenance_logical_address_interlock <= 1'b0;   // strobe
            maintenance_sector_pulse              <= 1'b0;
            maintenance_read_data_pulse           <= 1'b0;
            maintenance_read_clock_pulse          <= 1'b0;
            load_current_bus_address              <= 1'b0;
            load_word_count                       <= 1'b0;
            load_disk_address_register            <= 1'b0;
            
            if (address_match_comb & slave_write) begin
                slave_write_rply <= 1'b1;
                case ({slave_address[3:1],1'b0})
                    ADDRESS_OFFSET_RKDS : begin
                        // Read only register
                    end
                    ADDRESS_OFFSET_RKER : begin
                        // Read only register
                    end
                    ADDRESS_OFFSET_RKCS : begin
                        if (~slave_write_byte | (slave_write_byte & ~slave_address[0])) begin
                            go                               <= slave_wdata[0];
                            rk_function                      <= slave_wdata[3:1];
                            //current_bus_address_write[17:16] <= slave_wdata[5:4]; -- Currently unsupported
                            interrupt_on_done_enable         <= slave_wdata[6];
                        end
                        if (~slave_write_byte | (slave_write_byte & slave_address[0])) begin
                            stop_on_soft_error               <= slave_wdata[8];
                            //read_write_all                   <= slave_wdata[9]; -- Currently unsupported (RK HW only?=
                            format                           <= slave_wdata[10];
                            inhibit_incrementing_rkba        <= slave_wdata[11];
                            maintenance_mode                 <= slave_wdata[12];
                        end
                    end
                    ADDRESS_OFFSET_RKWC : begin
                        if (disk_state == DISK_STATE_IDLE) begin
                            load_word_count           <= 1'b1;
                            if (slave_write_byte & slave_address[0])
                                word_count_write          <= {slave_wdata[7:0], word_count_register[7:0]};
                            else if (slave_write_byte & ~slave_address[0])
                                word_count_write          <= {word_count_register[15:8], slave_wdata[7:0]};
                            else
                                word_count_write          <= slave_wdata;
                        end
                    end
                    ADDRESS_OFFSET_RKBA : begin
                        if (disk_state == DISK_STATE_IDLE) begin
                            load_current_bus_address        <= 1'b1;
                            if (slave_write_byte & slave_address[0])
                                current_bus_address_write          <= {slave_wdata[7:0], bus_address_register[7:0]};
                            else if (slave_write_byte & ~slave_address[0])
                                current_bus_address_write          <= {bus_address_register[15:8], slave_wdata[7:0]};
                            else
                                current_bus_address_write          <= slave_wdata;
                        end
                    end
                    ADDRESS_OFFSET_RKDA : begin
                        if (disk_state == DISK_STATE_IDLE) begin
                            load_disk_address_register  <= 1'b1;
                            if (slave_write_byte & slave_address[0])
                                disk_address_register_write          <= {slave_wdata[7:0], disk_address_register[7:0]};
                            else if (slave_write_byte & ~slave_address[0])
                                disk_address_register_write          <= {disk_address_register[15:8], slave_wdata[7:0]};
                            else
                                disk_address_register_write          <= slave_wdata;
                        end
                    end
                    ADDRESS_OFFSET_RKMR : begin
                        maintenance_sector_counter              <= slave_wdata[3:0];
                        maintenance_ready_to_read_write_or_seek <= slave_wdata[4];
                        maintenance_drive_ready                 <= slave_wdata[5];
                        maintenance_logical_address_interlock   <= slave_wdata[11];
                        maintenance_address_acknowledge         <= slave_wdata[12];
                        maintenance_sector_pulse                <= slave_wdata[13];
                        maintenance_read_data_pulse             <= slave_wdata[14];
                        maintenance_read_clock_pulse            <= slave_wdata[15];
                    end
                    ADDRESS_OFFSET_RKDB : begin
                        // Read only register
                    end
                endcase;
            end
        end
    end
    
    // -- Disk emulation process -- high level
    
    localparam DISK_STATE_IDLE          = 8'b0000_0001;
    localparam DISK_STATE_WRITE         = 8'b0000_0010;
    localparam DISK_STATE_READ          = 8'b0000_0100;
    localparam DISK_STATE_SEEK          = 8'b0000_1000;
    localparam DISK_STATE_DRIVE_RESET   = 8'b0001_0000;
    localparam DISK_STATE_CONTROL_RESET = 8'b0010_0000;
    localparam DISK_STATE_WRITE_LOCK    = 8'b0100_0000;
    
    reg [7:0] disk_state;
    reg check_only;
    
    reg start_write, start_read;
    reg write_done, read_done;
    
    wire load_sw_rst;
    wire load_dr_rst;
    
    assign load_sw_rst = go & (rk_function == 3'b000);
    assign load_dr_rst = go & (rk_function == 3'b110);
    
    always @(posedge clock) begin
        if (reset) begin
            sw_rst       <= 4'b0;
            dr_rst       <= 4'b0;
        end else begin
            sw_rst   <= (load_sw_rst) ? 4'b1111 : {sw_rst[2:0],1'b0};
            dr_rst   <= (load_dr_rst) ? 4'b1111 : {dr_rst[2:0],1'b0};
        end
    end
    
    
    // Control Process
    always @(posedge clock) begin
        if (ctrl_reset) begin
            control_ready <= 1'b1;
            start_write   <= 1'b0;
            start_read    <= 1'b0;
            irq           <= 1'b0;
            search_complete   <= 1'b0;
            disk_state    <= DISK_STATE_IDLE;
        end else begin
            start_write   <= 1'b0;
            start_read    <= 1'b0;
            irq           <= irq & ~iako & interrupt_on_done_enable;
            dbg[7]        <= dbg[7] | irq;
            dbg[6]        <= dbg[6] | iako;
            dbg[5]        <= dbg[5] | iako_rply;
            dbg[4:0]      <= control_status_register[4:0];
            
            case (disk_state)
                DISK_STATE_IDLE  : begin
                    if (go) begin
                        check_only <= rk_function[0];
                        control_ready <= 1'b0;
                        search_complete <= 1'b0;
                        case (rk_function)
                            3'b001, 3'b011 : begin                     // Write / Write All
                                start_write   <= 1'b1;
                                disk_state    <= DISK_STATE_WRITE;
                            end
                            3'b010, 3'b101 : begin                     // Read / Read All
                                start_read    <= 1'b1;
                                disk_state    <= DISK_STATE_READ;
                            end
                            3'b100 : begin
                                disk_state    <= DISK_STATE_SEEK;       // Position Arm
                                control_ready <= 1'b0;
                            end
                            3'b110 : begin
                                disk_state <= DISK_STATE_DRIVE_RESET;   // Reset Drive
                            end
                            3'b111 : begin
                                disk_state <= DISK_STATE_WRITE_LOCK;    // Write-protect Drive
                            end
                        endcase
                    end
                end 
                DISK_STATE_WRITE : begin
                    if (write_done) begin
                        control_ready <= 1'b1;
                        irq           <= interrupt_on_done_enable;
                        disk_state    <= DISK_STATE_IDLE;
                    end
                end
                DISK_STATE_READ  : begin
                    if (read_done) begin
                        control_ready <= 1'b1;
                        irq           <= interrupt_on_done_enable;
                        disk_state    <= DISK_STATE_IDLE;
                    end
                end
                DISK_STATE_SEEK  : begin
                    control_ready <= 1'b1;
                    irq           <= interrupt_on_done_enable;
                    disk_state    <= DISK_STATE_IDLE;
                    search_complete <= 1'b1;
                end
                DISK_STATE_DRIVE_RESET : begin
                    if (~drive_reset) begin
                        control_ready <= 1'b1;
                        irq           <= interrupt_on_done_enable;
                        disk_state    <= DISK_STATE_IDLE;
                    end
                    search_complete <= 1'b1;
                end
                DISK_STATE_CONTROL_RESET : begin
                    disk_state    <= DISK_STATE_IDLE;
                end
                DISK_STATE_WRITE_LOCK : begin
                    control_ready <= 1'b1;
                    irq           <= interrupt_on_done_enable;
                    disk_state    <= DISK_STATE_IDLE;
                end
            endcase
        end
    end
    
    reg continue_when_tx_ready;
    reg continue_when_dma_grant;
    reg continue_when_ar;
    reg continue_when_rply;
    reg continue_when_rx_ready;
    reg [15:0] transferred_data_words;
    reg rw;
    
    reg [18:0] low_level_state;
    localparam  LOW_LEVEL_STATE_IDLE =                 19'b000_0000_0000_0000_0001,
                LOW_LEVEL_STATE_XMIT_ADDR_LO =         19'b000_0000_0000_0000_0010,
                LOW_LEVEL_STATE_XMIT_ADDR_HI =         19'b000_0000_0000_0000_0100,
                LOW_LEVEL_STATE_XMIT_APPLY_DMR  =      19'b000_0000_0000_0000_1000,
                LOW_LEVEL_STATE_XMIT_APPLY_SACK =      19'b000_0000_0000_0001_0000,
                LOW_LEVEL_STATE_XMIT_APPLY_ADDRESS =   19'b000_0000_0000_0010_0000,
                LOW_LEVEL_STATE_XMIT_READ_WORD =       19'b000_0000_0000_0100_0000,
                LOW_LEVEL_STATE_XMIT_WAIT_TX =         19'b000_0000_0000_1000_0000,
                LOW_LEVEL_STATE_XMIT_WORD_SHIFT =      19'b000_0000_0001_0000_0000,
                LOW_LEVEL_STATE_XMIT_TX_LO_BYTE =      19'b000_0000_0010_0000_0000,
                LOW_LEVEL_STATE_XMIT_TX_HI_BYTE =      19'b000_0000_0100_0000_0000,
                LOW_LEVEL_STATE_RECV_WORD_ENQ =        19'b000_0000_1000_0000_0000,
                LOW_LEVEL_STATE_RECV_LO_BYTE =         19'b000_0001_0000_0000_0000,
                LOW_LEVEL_STATE_RECV_HI_BYTE =         19'b000_0010_0000_0000_0000,
                LOW_LEVEL_STATE_RECV_APPLY_SACK    =   19'b000_0100_0000_0000_0000,
                LOW_LEVEL_STATE_RECV_APPLY_ADDRESS =   19'b000_1000_0000_0000_0000,
                LOW_LEVEL_STATE_RECV_STORE_WORD =      19'b001_0000_0000_0000_0000,
                LOW_LEVEL_STATE_XFER_END =             19'b010_0000_0000_0000_0000,
                LOW_LEVEL_STATE_XFER_EOT =             19'b100_0000_0000_0000_0000;

    
    reg write_zero;
    reg [7:0] dbg;
    reg [7:0] wait_states;
    
        
    // -- Disk emulation process -- low level
    always @(posedge clock) begin
        if (drive_reset) begin
            tx_data                 <= 8'b0;
            tx_valid                <= 1'b0;
            write_zero              <= 1'b0;
            low_level_state         <= LOW_LEVEL_STATE_IDLE;
            rw                      <= 1'b0;
            tx_valid                <= 1'b0;
            continue_when_tx_ready  <= 1'b0;
            master_sync             <= 1'b0;
            master_dmr              <= 1'b0;
            master_dout             <= 1'b0;
            master_din              <= 1'b0;
            master_sack             <= 1'b0;
            master_ad               <= 16'b0;
            wait_states             <= 8'b0;
            
            continue_when_tx_ready  <= 1'b0;
            continue_when_dma_grant <= 1'b0;
            continue_when_rx_ready  <= 1'b0;
            continue_when_ar        <= 1'b0;
            continue_when_rply      <= 1'b0;
            
            word_count_register     <= 16'b0;
            bus_address_register    <= 18'b0;
            
            non_existent_cylinder <= 1'b0;
            non_existent_disk     <= 1'b0;
            non_existent_memory   <= 1'b0;
            non_existent_sector   <= 1'b0;
            transferred_data_words  <= 16'b0;
            overrun               <= 1'b0;
            
            disk_address_register[3:0]   <= 4'b0;  //   sector_address    =
            disk_address_register[4]     <= 1'b0;  //   surface           =
            disk_address_register[12:5]  <= 8'b0;  //   cylinder_address  =
            disk_address_register[15:13] <= 3'b0;  //   drive_select      =
            
            data_buffer <= 16'b0;
            sector_counter_OK <= 1'b1;
            
            read_done <= 1'b0;
            write_done <= 1'b0;
            
        end else begin
            tx_valid   <= 1'b0;
            read_done  <= 1'b0;
            write_done <= 1'b0;
                        
            if (load_current_bus_address) begin
                if (current_bus_address_write >= 16'o160000)
                    non_existent_memory  <= 1'b1;
                else begin
                    non_existent_memory  <= 1'b0;
                    bus_address_register <= {2'b0,current_bus_address_write};
                end
            end
            
            if (load_disk_address_register) begin
                if (disk_address_register_write[12:5] > 8'o312)
                    non_existent_cylinder <= 1'b1;
                else if (disk_address_register_write[3:0] > 8'o13)
                    non_existent_sector   <= 1'b1;
                else
                    disk_address_register  <= disk_address_register_write;
            end
            
            if (load_word_count) begin
                word_count_register <= word_count_write;
            end
            
            if (ce & wait_states != 8'b0)
                wait_states <= wait_states - 1;
            
            if ((((continue_when_tx_ready  & ~tx_busy & ~tx_valid) || (~continue_when_tx_ready) ) &&
                 ((continue_when_dma_grant & ce & master_dmgo)     || (~continue_when_dma_grant)) &&
                 ((continue_when_rx_ready  & rx_valid)             || (~continue_when_rx_ready) ) &&
                 ((continue_when_ar        & ce & master_ar)       || (~continue_when_ar)       ) &&
                 ((continue_when_rply      & ce & master_rply)     || (~continue_when_rply))    ) && wait_states == 8'b0) begin
                
                if (continue_when_rply & master_rply) begin
                    master_dmr  <= 1'b0;
                    master_dout <= 1'b0;
                    master_din  <= 1'b0;
                end
                
                if (continue_when_ar & master_ar) begin
                    master_sync <= 1'b0;
                end
                
                continue_when_tx_ready  <= 1'b0;
                continue_when_dma_grant <= 1'b0;
                continue_when_rx_ready  <= 1'b0;
                continue_when_ar        <= 1'b0;
                continue_when_rply      <= 1'b0;
                
                case (low_level_state)
                    LOW_LEVEL_STATE_IDLE : begin
                        rw <= start_write;
                        if ((start_write | start_read) & (~non_existent_memory & ~non_existent_sector & ~non_existent_cylinder)) begin
                            write_zero            <= 1'b0;
                            non_existent_cylinder <= 1'b0;
                            non_existent_disk     <= 1'b0;
                            non_existent_sector   <= 1'b0;
                            overrun               <= 1'b0;
                            transferred_data_words  <= 16'b0;
                            read_done <= 1'b0;
                            write_done <= 1'b0;
                            low_level_state        <= LOW_LEVEL_STATE_XMIT_ADDR_LO;
                            continue_when_tx_ready <= 1'b1;
                            
                        end
                    end
                    LOW_LEVEL_STATE_XMIT_ADDR_LO : begin
                        //           7...5                  4        3...0
                        tx_data  <= {cylinder_address[2:0], surface, sector_address};
                        tx_valid <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_ADDR_HI;
                        continue_when_tx_ready  <= 1'b1;
                    end
                    LOW_LEVEL_STATE_XMIT_ADDR_HI : begin
                        //           7...5              4...0
                        tx_data    <= {drive_select[2:0], cylinder_address[7:3]};
                        tx_valid   <= 1'b1;
                        if (rw) begin
                            master_dmr <= 1'b1;
                            // --
                            continue_when_dma_grant <= 1'b1;
                            low_level_state <= LOW_LEVEL_STATE_XMIT_APPLY_SACK;
                        end else begin
                            // --
                            continue_when_tx_ready <= 1'b1;
                            low_level_state <= LOW_LEVEL_STATE_RECV_WORD_ENQ;
                        end
                    end
                    LOW_LEVEL_STATE_XMIT_APPLY_DMR : begin
                        master_dmr <= 1'b1;
                        // --
                        continue_when_dma_grant <= 1'b1;
                        low_level_state <= LOW_LEVEL_STATE_XMIT_APPLY_SACK;
                    end
                    LOW_LEVEL_STATE_XMIT_APPLY_SACK : begin
                        master_sack             <= 1'b1;
                        wait_states             <= 4'h4;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_APPLY_ADDRESS;
                    end
                    LOW_LEVEL_STATE_XMIT_APPLY_ADDRESS : begin
                        master_sync             <= 1'b1;
                        master_ad               <= bus_address_register;
                        if (~inhibit_incrementing_rkba)
                            bus_address_register    <= bus_address_register + 16'h2;
                        // --
                        continue_when_ar        <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_READ_WORD;
                    end
                    LOW_LEVEL_STATE_XMIT_READ_WORD : begin
                        master_din              <= 1'b1;
                        // --
                        continue_when_rply      <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_WAIT_TX;
                    end
                    LOW_LEVEL_STATE_XMIT_WAIT_TX : begin
                        data_buffer             <= master_rdata;
                        //master_sack             <= 1'b0;
                        continue_when_tx_ready  <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_WORD_SHIFT;
                    end
                    LOW_LEVEL_STATE_XMIT_WORD_SHIFT : begin
                        tx_valid                <= 1'b1;
                        tx_data                 <= 8'h0F;   // [TRANSMIT] Send SI sync byte to host
                        continue_when_tx_ready  <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_TX_LO_BYTE;
                    end
                    LOW_LEVEL_STATE_XMIT_TX_LO_BYTE : begin
                        tx_valid <= 1'b1;
                        tx_data  <= (write_zero) ? 8'b0 : data_buffer[7:0];
                        continue_when_tx_ready  <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XMIT_TX_HI_BYTE;
                    end
                    LOW_LEVEL_STATE_XMIT_TX_HI_BYTE : begin
                        tx_valid                <= 1'b1;
                        if (write_zero) begin
                            tx_data             <= 8'b0;
                        end else begin
                            tx_data             <= data_buffer[15:8];
                            word_count_register <= word_count_register    + 16'h1;
                        end
                        transferred_data_words  <= transferred_data_words + 16'h1;
                        low_level_state         <= LOW_LEVEL_STATE_XFER_END;
                    end
                    LOW_LEVEL_STATE_RECV_WORD_ENQ : begin               // [RECEIVE] Send ENQ sync byte to host
                        tx_valid <= 1'b1;
                        tx_data  <= 8'h5; // ENQ
                        continue_when_rx_ready <= 1'b1;
                        low_level_state        <= LOW_LEVEL_STATE_RECV_LO_BYTE;
                    end
                    LOW_LEVEL_STATE_RECV_LO_BYTE: begin                 // [RECEIVE] Receive LO byte
                        data_buffer[7:0]       <= rx_data;
                        // --
                        continue_when_rx_ready <= 1'b1;
                        low_level_state        <= LOW_LEVEL_STATE_RECV_HI_BYTE;
                    end
                    LOW_LEVEL_STATE_RECV_HI_BYTE: begin                 // [RECEIVE] Receive HI byte, request DMA
                        data_buffer[15:8]       <= rx_data;
                        master_dmr              <= 1'b1;
                        // --
                        continue_when_dma_grant <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_RECV_APPLY_SACK;
                    end
                    LOW_LEVEL_STATE_RECV_APPLY_SACK : begin
                        master_sack             <= 1'b1;
                        wait_states             <= 4'h4;
                        //if (bus_address_register >= 16'o160000) begin // ignore writes to I/O page
                        //  word_count_register     <= word_count_register    + 16'h1;
                        //  transferred_data_words  <= transferred_data_words + 16'h1;
                        //  low_level_state         <= LOW_LEVEL_STATE_XFER_END;
                        //  end else
                        low_level_state         <= LOW_LEVEL_STATE_RECV_APPLY_ADDRESS;
                    end
                    LOW_LEVEL_STATE_RECV_APPLY_ADDRESS:begin            // [RECEIVE] Apply bus address
                        master_sync             <= 1'b1;
                        master_ad               <= bus_address_register;
                        if (~inhibit_incrementing_rkba)
                            bus_address_register    <= bus_address_register + 16'h2;
                        // --
                        continue_when_ar        <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_RECV_STORE_WORD;
                    end
                    LOW_LEVEL_STATE_RECV_STORE_WORD: begin
                        master_dout             <= 1'b1;
                        master_ad               <= data_buffer;
                        word_count_register     <= word_count_register    + 16'h1;
                        transferred_data_words  <= transferred_data_words + 16'h1;
                        // --
                        continue_when_rply      <= 1'b1;
                        low_level_state         <= LOW_LEVEL_STATE_XFER_END;
                    end
                    LOW_LEVEL_STATE_XFER_END : begin
                        master_sack             <= 1'b0;
                        
                        if (transferred_data_words[7:0] == 8'h0) begin
                            disk_address_register[3:0] <= disk_address_register[3:0] + 4'b1;
                            if (sector_address == 4'o13) begin
                                    // Done
                                    disk_address_register[12:4] <= disk_address_register[12:4] + 8'b1;
                                    disk_address_register[3:0]  <= 4'b0;
                            end
                        end
                        
                        if (word_count_register == 0) begin

                            if (rw && (transferred_data_words[7:0] != 8'h0)) begin
                                write_zero              <= 1'b1;
                                continue_when_tx_ready  <= 1'b1;
                                low_level_state         <= LOW_LEVEL_STATE_XMIT_WORD_SHIFT;
                            end else begin
                                continue_when_tx_ready  <= 1'b1;
                                low_level_state         <= LOW_LEVEL_STATE_XFER_EOT;
                            end
                        end else if (transferred_data_words[7:0] == 8'h0) begin
                            continue_when_tx_ready     <= 1'b1;
                            low_level_state            <= LOW_LEVEL_STATE_XFER_EOT;
                            if (cylinder_address == 8'o312 && surface == 1'b1 && sector_address == 4'o13) begin
                                // Done
                                continue_when_tx_ready  <= 1'b1;
                                // Overrun a single drive
                                overrun                <= 1'b1;
                            end
                        end else if (rw & ~write_zero) begin
                            // Write next word in sector
                            wait_states             <= 4'h10;
                            continue_when_tx_ready  <= 1'b1;
                            low_level_state         <= LOW_LEVEL_STATE_XMIT_APPLY_DMR;
                        end else if (rw & write_zero) begin
                            continue_when_tx_ready  <= 1'b1;
                            low_level_state         <= LOW_LEVEL_STATE_XMIT_WORD_SHIFT;
                        end else begin
                            // Read next word in sector
                            continue_when_tx_ready <= 1'b1;
                            low_level_state        <= LOW_LEVEL_STATE_RECV_WORD_ENQ;
                        end
                        // -- Error conditions
                        if (word_count_register != 0 && bus_address_register >= 16'o160000) begin
                            non_existent_memory     <= 1'b1;
                            continue_when_tx_ready  <= 1'b1;
                        //    dbg                     <= 8'hff;
                            low_level_state         <= LOW_LEVEL_STATE_XFER_EOT;
                        end 
                    end
                    LOW_LEVEL_STATE_XFER_EOT : begin
                        // --
                        tx_valid <= 1'b1;
                        tx_data  <= 8'h04;      // [TRANSMIT] Send EOT byte to host
                        // Done
                        if (non_existent_memory || overrun || (word_count_register == 0)) begin // end of transfer
                            write_done            <= rw;
                            read_done             <= ~rw;
                            low_level_state       <= LOW_LEVEL_STATE_IDLE;
                        end else begin                                                        // re-sync drive address
                            continue_when_tx_ready  <= 1'b1;
                            low_level_state         <= LOW_LEVEL_STATE_XMIT_ADDR_LO;
                        end
                    end
                endcase
            end
        end
    end

    // -- Interrupt rply process
    always @(posedge clock) begin
        if (reset) begin
            iako_rply      <= 1'b0;
        end else begin
            iako_rply      <= iako_rply & iako;
            if (iako) begin
                iako_rply <= 1'b1;
            end
        end
    end
    
    // Reply signals
    assign slave_rply = slave_read_rply | slave_write_rply;
    assign debug = {master_dmgo,master_dmr,master_sack,master_dout,master_din,master_rply};

endmodule
