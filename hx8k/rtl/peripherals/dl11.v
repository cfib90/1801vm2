`default_nettype none
module dl11b #(
    parameter BASE_ADDRESS    = 16'o177560,  // -- Default: System Console (Teletype)
    parameter RCVR_INT_VECTOR = 16'o000060,  // -- Default: Teletype IN
    parameter XMIT_INT_VECTOR = 16'o000064,  // -- Default: Teletype OUT
    parameter PC11            = 0
) (
    input  wire clock,
    input  wire reset,
    // -- bus
    input  wire         write,
    input  wire         read,
    input  wire         write_byte,
    input  wire         din,
    input  wire         dout,
    input  wire [15:0]  address,
    input  wire [15:0]  wdata,
    output reg  [15:0]  rdata,
    output wire         rply,
    output wire         addr_match,
    output wire         ready,
    
    // -- interrupt
    output reg          irq,
    output wire [15:0]  irq_vector,
    output reg          iako_rply,
    input  wire         iako,
    
    // -- I/O
    output reg  [7:0]   tx_data,
    output reg          tx_valid,
    input  wire         tx_busy,
    
    input wire  [7:0]   rx_data,
    input wire          rx_valid
);

    // -----------------------------------------------------------------
    // --Register interface
    // -----------------------------------------------------------------
    
    // -- Register +0 : Receiver Status Register (RCSR)
    
    localparam ADDRESS_OFFSET_RCSR   = 3'o0;
    
    localparam RCSR_RCVR_ACT_BIT     = 11,
               RCSR_RCVR_DONE_BIT    = 7,
               RCSR_RCVR_INT_ENB_BIT = 6,
               RCSR_RCVR_RDR_ENB_BIT = 0;

    reg rcsr_rcvr_act;              // -- [RO] Receiver Active: When set, this bit indicates that the DL11 interface receiver is active. cleared by INIT or by RCVR DONE
    reg rcsr_rcvr_done;             // -- [RO] This bit is set when an entire character has been received and is ready for transfer to the Bus. When set, initiates an
                                    //         interrupt sequence provided RCVR_INT_ENB is also set. Cleared whenever the receiver buffer RBUF is addressed or
                                    //         whenever RDR ENB is set. Also cleared by INIT.
    reg rcsr_rcvr_int_enb;          // -- [RW] When set, allows an interrupt sequence to start when RCVR DONE sets
    reg rcsr_rcvr_rdr_enb;          // -- [WO] When set, this bit advances the paper tape reader in ASR teletype units and clears the RCVR DONEE bit.
    
    wire [15:0] receiver_status_register;
    assign receiver_status_register[RCSR_RCVR_ACT_BIT]      = rcsr_rcvr_act;
    assign receiver_status_register[RCSR_RCVR_DONE_BIT]     = rcsr_rcvr_done;
    assign receiver_status_register[RCSR_RCVR_INT_ENB_BIT]  = rcsr_rcvr_int_enb;
    assign receiver_status_register[RCSR_RCVR_RDR_ENB_BIT]  = rcsr_rcvr_rdr_enb;
    assign receiver_status_register[15:RCSR_RCVR_ACT_BIT+1] = 4'b0;
    assign receiver_status_register[10:8]                   = 3'b0;
    assign receiver_status_register[5:1]                    = 4'b0;
    
    // -- Register +2 : Receiver Buffer Register (RBUF)

    localparam ADDRESS_OFFSET_RBUF   = 3'o2;

    localparam RBUF_RECEIVED_DATA_BITS_BIT = 0,
               RBUF_RECEIVED_DATA_BITS_WIDTH = 8;
               
    reg [7:0] received_data_bits;   // -- [RO] Holds the character just read
    wire [15:0] receiver_buffer_register;
    
    assign receiver_buffer_register[RBUF_RECEIVED_DATA_BITS_BIT+:RBUF_RECEIVED_DATA_BITS_WIDTH] = received_data_bits;
    assign receiver_buffer_register[15:RBUF_RECEIVED_DATA_BITS_BIT+RBUF_RECEIVED_DATA_BITS_WIDTH] = {(15-RBUF_RECEIVED_DATA_BITS_WIDTH+1){1'b0}};
    
    // -- Register +4 : Transmitter Status Register (RBUF)
    
    localparam ADDRESS_OFFSET_XCSR   = 3'o4;
    
    localparam  XCSR_XMIT_RDY_BIT       = 7,
                XCSR_XMIT_INT_ENB_BIT   = 6,
                XCSR_MAINT_BIT          = 5;
                
    reg xcsr_xmit_rdy;            // -- [RO] This bit is set when the transmitter buffer XBUF can accept another character. When set, it initiates an interrupt sequence
                                   //         provided XMIT INT ENB is also set. Cleared by loading the transmitter buffer.
    reg xcsr_xmit_int_enb;         // -- [RW] When set, allows an interrupt sequence to start when XMIT RDY sets
    reg xcsr_maint;                // -- [RW] Used for maintenance function. When set, disables the serial line input to the receiver and
                                   //         connects the transmitter output to the receiver input which disconnects the external device input.
                                   
    wire [15:0] transmitter_status_register;
    
    assign transmitter_status_register[XCSR_XMIT_RDY_BIT]      = xcsr_xmit_rdy;
    assign transmitter_status_register[XCSR_XMIT_INT_ENB_BIT]  = xcsr_xmit_int_enb;
    assign transmitter_status_register[XCSR_MAINT_BIT]         = xcsr_maint;
    assign transmitter_status_register[15:XCSR_XMIT_RDY_BIT+1] = 8'b0;
    assign transmitter_status_register[XCSR_MAINT_BIT-1:0]     = 5'b0;

    // -- Register +6 : Transmitter Buffer Register (XBUF)
    
    localparam ADDRESS_OFFSET_XBUF   = 3'o6;

    localparam XBUF_TRANSMITTER_DATA_BUFFER_BIT   = 0,
               XBUF_TRANSMITTER_DATA_BUFFER_WIDTH = 8;

    reg [7:0] transmitter_data_buffer;
    
    // -- Address match
    
    wire address_match_comb;
    assign address_match_comb = address[15:3] == BASE_ADDRESS[15:3];
    assign addr_match = address_match_comb;
    
    // -- Read process
    reg read_rply;
    reg buffer_read;
    wire rx_intr;
    wire tx_intr;
    wire irq_en;
    
    assign rx_intr = rcsr_rcvr_done & rcsr_rcvr_int_enb;
    assign tx_intr = xcsr_xmit_rdy  & xcsr_xmit_int_enb;
    assign irq_en  = rx_intr | tx_intr;
    reg irq_en_dly;
    
    assign irq_vector = (rx_intr) ? RCVR_INT_VECTOR : XMIT_INT_VECTOR;
        
    always @(posedge clock) begin
        if (reset) begin
            read_rply         <= 1'b0;
            rdata             <= 16'b0;
            buffer_read       <= 1'b0;
            rcsr_rcvr_done    <= 1'b0;
            irq               <= 1'b0;
            irq_en_dly        <= 1'b0;
        end else begin
            buffer_read    <= 1'b0;
            read_rply      <= read_rply & din;
            rcsr_rcvr_act  <= (rcsr_rcvr_act | rcsr_rcvr_rdr_enb) & ~rcsr_rcvr_done;
            rcsr_rcvr_done <= (rcsr_rcvr_done | rx_valid) & ~buffer_read & ~rcsr_rcvr_rdr_enb;
            irq_en_dly     <= irq_en;
            
            irq            <= (irq | (irq_en & ~irq_en_dly)) & ~iako;
            
            if (address_match_comb & read) begin
                read_rply   <= 1'b1;
                case (address[2:0])
                    ADDRESS_OFFSET_RCSR : begin
                        rdata <= receiver_status_register;
                    end
                    ADDRESS_OFFSET_RBUF : begin
                        buffer_read <= 1'b1;
                        rdata <= receiver_buffer_register;
                    end
                    ADDRESS_OFFSET_XCSR : begin
                        rdata <= transmitter_status_register;
                    end
                    ADDRESS_OFFSET_XBUF : begin
                        rdata <= 16'b0;         // -- register is write-only
                    end
                endcase;
            end
        end
    end
    
    // -- Write process
    reg write_rply;
    reg rx_valid_dly;
    
    always @(posedge clock) begin
        if (reset) begin
            write_rply        <= 1'b0;
            rcsr_rcvr_rdr_enb <= 1'b0;
            tx_valid          <= 1'b0;
            tx_data           <= 8'b0;
            xcsr_maint        <= 1'b0;
            xcsr_xmit_int_enb <= 1'b0;
            xcsr_xmit_rdy     <= 1'b0;
            rx_valid_dly      <= 1'b0;
        end else begin
            write_rply        <= write_rply & dout;
            rcsr_rcvr_rdr_enb <= 1'b0;
            tx_valid          <= 1'b0;
            xcsr_xmit_rdy     <= ~tx_busy;
            rx_valid_dly      <= rx_valid;
            if (~rx_valid_dly & rx_valid)
                received_data_bits <= rx_data;

            // If in PC11 reading mode, request next byte by sending a single character
            if (PC11) begin
                if (rcsr_rcvr_rdr_enb) begin
                    xcsr_xmit_rdy       <= 1'b0;
                    tx_valid            <= 1'b1;
                    tx_data             <= 8'h05; // Enquiry character, cool!
                end
            end

            if (address_match_comb & write) begin
                write_rply <= 1'b1;
                case (address[2:0])
                    ADDRESS_OFFSET_RCSR : begin
                        rcsr_rcvr_int_enb <= wdata[RCSR_RCVR_INT_ENB_BIT];
                        rcsr_rcvr_rdr_enb <= wdata[RCSR_RCVR_RDR_ENB_BIT];
                    end
                    ADDRESS_OFFSET_RBUF : begin
                        // -- nothing to be done here, read-only
                    end
                    ADDRESS_OFFSET_XCSR : begin
                        xcsr_xmit_int_enb   <= wdata[XCSR_XMIT_INT_ENB_BIT];
                        xcsr_maint          <= wdata[XCSR_MAINT_BIT];
                    end
                    ADDRESS_OFFSET_XBUF : begin
                        xcsr_xmit_rdy       <= 1'b0;
                        tx_valid            <= 1'b1;
                        tx_data             <= wdata[XBUF_TRANSMITTER_DATA_BUFFER_BIT+:XBUF_TRANSMITTER_DATA_BUFFER_WIDTH];
                    end
                endcase;
            end
        end
    end


    // -- Interrupt rply process
    
    always @(posedge clock) begin
        if (reset) begin
            iako_rply      <= 1'b0;
        end else begin
            iako_rply      <= iako_rply & iako;
            if (iako) begin
                iako_rply <= 1'b1;
            end
        end
    end
    
    // Reply signals
    assign ready = tx_valid;
    assign rply = read_rply | write_rply;

endmodule
