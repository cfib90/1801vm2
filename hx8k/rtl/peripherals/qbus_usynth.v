`default_nettype none
module qbus_usynth #(
    parameter BASE_ADDRESS    = 16'o176000  // --
) (
    input  wire clock,
    input  wire reset,
    // -- bus
    input  wire         write,
    input  wire         read,
    input  wire         write_byte,
    input  wire         din,
    input  wire         dout,
    input  wire [15:0]  address,
    input  wire [15:0]  wdata,
    output reg  [15:0]  rdata,
    output wire         rply,
    output wire         addr_match,
    output wire         ready,
    
    // -- I/O
    output wire         pwm
);

    wire address_match_comb;
    assign address_match_comb = address[15:7] == BASE_ADDRESS[15:7];
    assign addr_match = address_match_comb;

    reg read_rply;
    reg write_rply;

    reg [3:0] enable;
    reg [3:0] play_note;
    reg [47:0] freq;
    reg [47:0] a;
    reg [47:0] d;
    reg [47:0] s;
    reg [47:0] r;
    reg [3:0] envelope;

    integer i;

    usynth #(.NUM_VOICES(3), .WAVEFORM(2)) synth_i 
             (.clock(clock),
              .ce25(1'b1),
              .reset(reset),
              .enable(enable[2:0]),
              .play_note(play_note[2:0]),
              .freq(freq),
              .pwm(pwm),
              .a(a), .d(d), .s(s), .r(r),
              .envelope(envelope[2:0]));

    always @(posedge clock) begin
        if (reset) begin
            write_rply        <= 1'b0;
            for (i=0;i<3;i=i+1) begin
                freq [i*16+:16] <= 440;
                a[i*16+:16]     <= 16'h400;
                d[i*16+:16]     <= 16'h400;
                s[i*16+:16]     <= 16'h8000;
                r[i*16+:16]     <= 16'h0100;
                enable[i]       <= 1'b0;
                play_note[i]    <= 1'b0;
                envelope[i]     <= 1'b0;
            end
        end else begin
            write_rply        <= write_rply & dout;

            if (address_match_comb & write) begin
                write_rply <= 1'b1;
                if (address[6]) begin
                        enable[address[2:1]]       <= wdata[0];
                        play_note[address[2:1]]    <= wdata[1];
                        envelope[address[2:1]]     <= wdata[2];
                end else begin
                    case (address[3:1])
                        3'h0: freq  [address[5:4]*16+:16] <= {wdata};
                        3'h2: a     [address[5:4]*16+:16] <= {wdata};
                        3'h4: d     [address[5:4]*16+:16] <= {wdata};
                        3'h6: s     [address[5:4]*16+:16] <= {wdata};
                        4'h8: r     [address[5:4]*16+:16] <= {wdata};
                        default : play_note <= play_note;
                    endcase
                end
            end
        end
    end

    always @(posedge clock) begin
        if (reset) begin
            read_rply         <= 1'b0;
            rdata             <= 16'b0;
        end else begin
            read_rply      <= read_rply & din;
            
            if (address_match_comb & read) begin
                read_rply   <= 1'b1;
            end
        end
    end

    assign rply = read_rply | write_rply;


endmodule
