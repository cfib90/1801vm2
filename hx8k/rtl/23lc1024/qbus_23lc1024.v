`default_nettype none
module qbus_23lc1024 #(
    parameter BASE_ADDRESS    = 16'o000000  // -- Default: Main Memory
) (
    input  wire clock,
    input  wire reset,
    // -- bus
    input  wire         write,
    input  wire         read,
    input  wire         write_byte,
    input  wire         din,
    input  wire         dout,
    input  wire [15:0]  address,
    input  wire [15:0]  wdata,
    output reg  [15:0]  rdata,
    output wire         rply,
    output wire         addr_match,
    output wire         ready,
    
    // -- I/O
    input  wire [3:0] sio_i,
    output wire [3:0] sio_o,
    output wire [3:0] sio_oen,
    output wire cs_n,
    output wire sck);

    // -- Address match: 48k
    
    wire address_match_comb;
    assign address_match_comb = (address[15:12] < 4'he);
    assign addr_match = address_match_comb;
    assign ready = (mem_write==2'b01) && (mem_addr[15:0] == 16'o2002);

    reg [1:0] mem_write;
    reg mem_read;
    reg [23:0] mem_addr;
    reg [15:0] mem_wdata;

    reg read_rply, write_rply;
    assign rply = read_rply | write_rply;

    reg [2:0] state;
    localparam STATE_IDLE = 3'b001,
               STATE_WRITE = 3'b010,
               STATE_READ  = 3'b100;

    always @(posedge clock) begin
        if (reset) begin
            read_rply      <= 1'b0;
            write_rply     <= 1'b0;
            state          <= STATE_IDLE;
            rdata          <= 16'hffff;
            mem_read       <= 1'b0;
            mem_write      <= 2'b0;
            mem_addr       <= 24'b0;
        end else begin
            read_rply      <= read_rply   & din;
            write_rply     <= write_rply  & dout;
            case (state)
                STATE_IDLE : begin
                    mem_addr     <= {8'b0,address[15:0]};
                    if (address_match_comb & read) begin
                        state      <= STATE_READ;
                        mem_read   <= 1'b1;
                        read_rply  <= 1'b0;
                    end else if (address_match_comb & write) begin
                        mem_write[0] <= ((~write_byte) | (write_byte & (~address[0])));
                        mem_write[1] <= ((~write_byte) | (write_byte & address[0]));
                        state      <= STATE_WRITE;
                        mem_wdata  <= wdata;
                        write_rply <= 1'b0;
                    end
                end
                STATE_READ : begin
                    mem_read    <= 1'b0;
                    if (done) begin
                        rdata       <= mem_rdata;
                        read_rply   <= 1'b1;
                        state       <= STATE_IDLE;
                    end
                end
                STATE_WRITE : begin
                    mem_write   <= 2'b0;
                    if (done) begin
                        write_rply  <= 1'b1;
                        state       <= STATE_IDLE;
                    end
                end
            endcase
        end
    end

    // -- MEMIF
    wire [15:0] mem_rdata;
    wire busy;
    wire done;

   mem_23lc1024 mem (.clock(clock),
                     .reset(reset),
                     
                     .write(mem_write),
                     .wdata(mem_wdata),
                     .read(mem_read),
                     .rdata(mem_rdata),
                     .addr(mem_addr),
                     .done(done),
                     .busy(busy),
                     .sio_i(sio_i),
                     .sio_o(sio_o),
                     .sio_oen(sio_oen),
                     .cs_n(cs_n),
                     .sck(sck));


endmodule
