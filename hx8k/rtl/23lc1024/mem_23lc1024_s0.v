`default_nettype none
module mem_23lc1024 (input wire clock,
                     input wire reset,
                     
                     input  wire [1:0]  write,
                     input  wire [15:0] wdata,
                     input  wire        read,
                     output reg  [15:0] rdata,
                     input  wire [23:0] addr,
                     output wire        done,
                     output wire        busy,

                     output reg [3:0] sio_o,
                     input  wire [3:0] sio_i,
                     output reg [3:0] sio_oen,
                     output reg  cs_n,
                     output wire sck);
    integer i;

    reg [6:0] serial_phase;

    always @(posedge clock) begin
        if (reset) begin
            serial_phase <= 7'b0;
        end else begin
            serial_phase <= serial_phase + 1;
        end
    end

    wire serial_phase_0, serial_phase_1, serial_phase_2, serial_phase_3;

    assign serial_phase_0 = (serial_phase[3:0] == 7'b0000);
    assign serial_phase_1 = (serial_phase[3:0] == 7'b0100);
    assign serial_phase_2 = (serial_phase[3:0] == 7'b1000);
    assign serial_phase_3 = (serial_phase[3:0] == 7'b1100);

    reg sck_en;

    assign sck  = (serial_phase[3] ^ serial_phase[2]) & sck_en;

    reg [5:0] tx_state;
    
    localparam TX_STATE_INIT   = 6'b000001,
               TX_STATE_IDLE   = 6'b000010,
               TX_STATE_WAIT   = 6'b000100,
               TX_STATE_SELECT = 6'b001000,
               TX_STATE_XMIT   = 6'b010000,
               TX_STATE_DONE   = 6'b100000;

    reg [5:0] tx_cnt;
    reg rx_flag;

    reg [47:0] tx_buffer;
    reg [15:0] rx_buffer;
    reg [3:0]  sio_oen_reg; 

    assign busy = (tx_state != TX_STATE_IDLE);
    assign done = (tx_state == TX_STATE_DONE);

    always @(posedge clock) begin
        if (reset) begin
            sck_en      <= 1'b0;
            cs_n        <= 1'b0;
            sio_oen     <= 4'b1101;
            sio_oen_reg <= 4'b1101;
            sio_o       <= 4'b1111;
            tx_cnt      <= 4'b0;
            rx_flag     <= 1'b0;
            tx_state    <= TX_STATE_INIT;
        end else begin
            sio_o[3:1]  <= 3'b111;
            if (serial_phase_3 && (tx_state == TX_STATE_SELECT || tx_state == TX_STATE_XMIT))
                tx_cnt <= tx_cnt - 1;

            if (tx_state == TX_STATE_INIT) begin

                tx_cnt                <= 9;
                tx_buffer[47:40]      <= {8'hff};
                tx_state              <= (serial_phase_3) ? TX_STATE_SELECT : TX_STATE_WAIT;

            end else if (tx_state == TX_STATE_IDLE) begin

                rx_flag <= 1'b0;

                if (read) begin
                    tx_cnt             <= 49;
                    rx_flag            <= 1'b1;
                    tx_state           <= (serial_phase_3) ? TX_STATE_SELECT : TX_STATE_WAIT;
                    // -- Load address
                    tx_buffer[47:40]      <= {8'h03};
                    tx_buffer[39:16]      <= {addr[15:1],1'b0};
                    rx_buffer[15:0]       <= 16'b0;

                end else if (write == 2'b01) begin
                    tx_cnt             <= 41;
                    tx_state           <= (serial_phase_3) ? TX_STATE_SELECT : TX_STATE_WAIT;
                    // -- Load address
                    tx_buffer[47:40]      <= {8'h02};
                    tx_buffer[39:16]      <= addr;

                    // -- Load writedata
                    tx_buffer[15:8]       <= wdata[7:0];

                end else if (write == 2'b10) begin
                    tx_cnt             <= 41;
                    tx_state           <= (serial_phase_3) ? TX_STATE_SELECT : TX_STATE_WAIT;
                    // -- Load address
                    tx_buffer[47:40]      <= {8'h02};
                    tx_buffer[39:16]      <= addr;

                    // -- Load writedata
                    tx_buffer[15:8]       <= wdata[15:8];

                end else if (write == 2'b11) begin
                    tx_cnt             <= 49;
                    tx_state           <= (serial_phase_3) ? TX_STATE_SELECT : TX_STATE_WAIT;
                    // -- Load address
                    tx_buffer[47:40]      <= {8'h02};
                    tx_buffer[39:16]      <= addr;

                    // -- Load writedata
                    tx_buffer[15:0]       <= {wdata[7:0],wdata[15:8]};
                end

            end else if (tx_state == TX_STATE_WAIT) begin
                if (serial_phase_3) begin
                    tx_state <= TX_STATE_SELECT;
                end
            end else if (tx_state == TX_STATE_SELECT) begin
                if (serial_phase_1)
                    cs_n     <= 1'b0;
                else if (serial_phase_3) begin
                    sio_oen  <= sio_oen_reg;
                    tx_state <= TX_STATE_XMIT;
                    sck_en   <= 1'b1;

                    sio_o[0]     <= tx_buffer[47];
                    tx_buffer    <= {tx_buffer[46:0],1'b0};
                end

            end else if (tx_state == TX_STATE_XMIT) begin

                // Shift out next nibble
                if (serial_phase_3) begin
                    sio_o[0]     <= tx_buffer[47];
                    tx_buffer    <= {tx_buffer[46:0],1'b0};
                    
                    if (tx_cnt == 1) begin
                        sio_oen <= 4'b0;
                        sck_en  <= 1'b0;
                    end
                end

                if (serial_phase_2 & tx_cnt > 0)
                    rx_buffer <= {rx_buffer[14:0],sio_i[1]};

                if (tx_cnt == 4'b0) begin
                    if (serial_phase_1)
                        cs_n     <= 1'b1;
                    if (serial_phase_3) begin
                        rdata    <= {rx_buffer[7:0],rx_buffer[15:8]};
                        tx_state <= TX_STATE_DONE;
                    end
                end

            end else if (tx_state == TX_STATE_DONE) begin
                tx_state <= TX_STATE_IDLE;
            end
        end
    end

endmodule
