`default_nettype none
module k1801vm2_board_test (input  wire osc,
                      // -- 1801vm2 signals
                     output wire [15:0] ad_n,
                     output wire sp1_n,
                     output wire sp2_n,
                     output wire dmr_n,
                     output wire sack_n,
                     output wire dmgo_n,
                     output wire clco,
                     output wire clci,
                     output wire rply_n,
                     output wire dout_n,
                     output wire wtbt_n,
                     output wire sync_n,
                     output wire din_n,
                     output wire ar_n,
                     output wire iako_n,
                     output wire aclo_n,
                     output wire dclo_n,
                     output wire init_n,
                     output wire virq_n,
                     output wire halt_n,
                     output wire evnt_n,
                     output wire sel_n,
                     output wire [7:0] led,
                     // -- board psu signal
                     input  wire pwr_5v_good,
                     // -- serial signals
                     output wire tx,
                     input wire rx
                     );

    reg [2:0] reset_reg;
    reg [2:0] pwrgood_reg;
    reg [23:0] cnt;
    wire dv;
    wire [7:0] data;
    reg [7:0] leddata;
    reg [40:0] selwire;
    wire reset;
    
    initial begin
        reset_reg <= 3'b1;
    end
    
    always @(posedge osc) begin
        reset_reg <= {reset_reg[1:0],1'b0};
    end
    
    assign reset = reset_reg[2];
    
    always @(posedge osc) begin
        if (reset) begin
            cnt <= 24'b0;
            pwrgood_reg <= 3'b0;
            selwire <= 41'b0;
        end else begin
            cnt <= cnt + 1;
            pwrgood_reg <= {pwrgood_reg[1:0],pwr_5v_good};
            if (dv) begin
                selwire       <= 41'b0;
                selwire[data] <= 1'b1;
                leddata <= data;
            end
        end
    end
    
    assign ad_n[0]  = selwire[9]  ? cnt[7] : 1'b1;
    assign ad_n[1]  = selwire[8]  ? cnt[7] : 1'b1;
    assign ad_n[2]  = selwire[7]  ? cnt[7] : 1'b1;
    assign ad_n[3]  = selwire[6]  ? cnt[7] : 1'b1;
    assign ad_n[4]  = selwire[5]  ? cnt[7] : 1'b1;
    assign ad_n[5]  = selwire[4]  ? cnt[7] : 1'b1;
    assign ad_n[6]  = selwire[3]  ? cnt[7] : 1'b1;
    assign ad_n[7]  = selwire[2]  ? cnt[7] : 1'b1;
    assign ad_n[8]  = selwire[39] ? cnt[7] : 1'b1;
    assign ad_n[9]  = selwire[38] ? cnt[7] : 1'b1;
    assign ad_n[10] = selwire[37] ? cnt[7] : 1'b1;
    assign ad_n[11] = selwire[36] ? cnt[7] : 1'b1;
    assign ad_n[12] = selwire[35] ? cnt[7] : 1'b1;
    assign ad_n[13] = selwire[34] ? cnt[7] : 1'b1;
    assign ad_n[14] = selwire[33] ? cnt[7] : 1'b1;
    assign ad_n[15] = selwire[32] ? cnt[7] : 1'b1;
    assign sp1_n    = selwire[10] ? cnt[7] : 1'b1;
    assign sp2_n    = selwire[11] ? cnt[7] : 1'b1;
    assign dmr_n    = selwire[12] ? cnt[7] : 1'b1;
    assign sack_n   = selwire[13] ? cnt[7] : 1'b1;
    assign dmgo_n   = selwire[14] ? cnt[7] : 1'b1;
    assign clco     = selwire[15] ? cnt[7] : 1'b1;
    assign clci     = selwire[16] ? cnt[7] : 1'b1;
    assign rply_n   = selwire[17] ? cnt[7] : 1'b1;
    assign dout_n   = selwire[18] ? cnt[7] : 1'b1;
    assign wtbt_n   = selwire[19] ? cnt[7] : 1'b1;
    assign sync_n   = selwire[21] ? cnt[7] : 1'b1;
    assign din_n    = selwire[22] ? cnt[7] : 1'b1;
    assign ar_n     = selwire[23] ? cnt[7] : 1'b1;
    assign iako_n   = selwire[24] ? cnt[7] : 1'b1;
    assign aclo_n   = selwire[25] ? cnt[7] : 1'b1;
    assign dclo_n   = selwire[26] ? cnt[7] : 1'b1;
    assign init_n   = selwire[27] ? cnt[7] : 1'b1;
    assign virq_n   = selwire[28] ? cnt[7] : 1'b1;
    assign halt_n   = selwire[29] ? cnt[7] : 1'b1;
    assign evnt_n   = selwire[30] ? cnt[7] : 1'b1;
    assign sel_n    = selwire[31] ? cnt[7] : 1'b1;

    assign led     = leddata;
    assign tx      = 1'b1;

    wire ce16;
    wire	[11:0]	baud_freq;
    wire	[15:0]	baud_limit;
    
    assign baud_freq = 96;
    assign baud_limit = 529;

    baud_gen bg (osc,reset,ce16,baud_freq,baud_limit);
    uart_rx srx (osc,reset,ce16,rx,data,dv);

endmodule
