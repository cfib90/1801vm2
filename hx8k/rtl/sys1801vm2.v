`default_nettype none
// 1801vm2 system. QBUS Backbone & Peripheral instantiations
module sys1801vm2 (
        input wire clk,
        input wire reset,
        input wire ce,
        input wire ce_clci,
        input wire ce_50hz,
        // -- 1801vm2 bidirs
        input  wire [15:0] ad_i,
        output reg  [15:0] ad_o,
        output reg  ad_oe,
        // -- 1801vm2 inputs
        input wire sp1,
        input wire dmgo,
        input wire dout,
        input wire wtbt,
        input wire sync,
        input wire din,
        input wire iako,
        input wire init,
        input wire sel,
        input wire pwr_5v_good,
        // -- 1801vm2 outputs
        output reg sp2,
        output reg dmr,
        output reg sack,
        output reg rply,
        output reg ar,
        output reg aclo,
        output reg dclo,
        output wire virq,
        output reg halt,
        output reg evnt,
        // -- serial
        input wire rx,
        output wire tx,
        // -- SPIMEM

        input  wire [3:0] sio_i,
        output wire [3:0] sio_o,
        output wire [3:0] sio_oen,
        output wire cs_n,
        output wire sck,

        // -- Audio
        output wire pwm,
        // -- dbg
        output wire [7:0] dbg
        );
        
        
    localparam UART_FUNCTION_QBUS_MONITOR = 0,
               UART_FUNCTION_TERMINAL     = 1;
        
    localparam UART_FUNCTION = UART_FUNCTION_TERMINAL;

    reg pwr_5v_good_flag;
    reg [19:0] pwr_5v_good_cnt;
    reg [15:0]  aclo_reg;
    wire sys1801vm2_reset;
    reg dclo_dly, wait_init;
    reg sel_dly, din_dly, dout_dly;

    initial begin
        sp2  <= 1'b1;
        dmr  <= 1'b0;
        sack <= 1'b0;
        rply <= 1'b0;
        ar   <= 1'b0;
        aclo <= 1'b1;
        dclo <= 1'b1;
        halt <= 1'b0;
        evnt <= 1'b0;
        ad_o <= 16'b0;
        ad_oe <= 1'b0;
        pwr_5v_good_cnt <= 19'h7ffff;
    end

    // -- Reset Generator
    // -- 5v status/reset
    // -- aclo/dclo gen
    
    assign sys1801vm2_reset = reset | ~pwr_5v_good_flag | init;

    always @(posedge clk) begin
        if (reset | ~pwr_5v_good) begin
            pwr_5v_good_cnt  <= 19'hff;
            pwr_5v_good_flag <= 1'b0;
            dclo             <= 1'b1;
            aclo             <= 1'b1;
        end else if (ce_clci & aclo) begin
            pwr_5v_good_cnt <= pwr_5v_good_cnt - 1;
            if (pwr_5v_good_cnt == 19'hf0) begin
                pwr_5v_good_flag <= 1'b1;
                dclo             <= 1'b0;
            end else if (pwr_5v_good_cnt == 19'h80) begin
                aclo             <= 1'b0;
            end
        end
    end

    // -----------------------------------------------------------------
    // UART
    
    wire [7:0] uart_tx_data;
    wire       uart_tx_valid;
    wire       uart_tx_busy;
    wire [7:0] uart_rx_data;
    wire       uart_rx_valid;
    
    wire [7:0] dbg_tx_data;
    wire       dbg_tx_valid;
    wire       dbg_tx_busy;
    wire [7:0] dbg_rx_data;
    wire       dbg_rx_valid;
    
    wire [7:0] term_tx_data;
    wire       term_tx_valid;
    wire       term_tx_busy;
    wire [7:0] term_rx_data;
    wire       term_rx_valid;
    
    wire [7:0] pc11_tx_data;
    wire       pc11_tx_valid;
    wire       pc11_tx_busy;
    wire [7:0] pc11_rx_data;
    wire       pc11_rx_valid;
    
    wire [31:0] mux_tx_data;
    wire [3:0]  mux_tx_valid;
    wire [3:0]  mux_tx_busy;
    wire [31:0] mux_rx_data;
    wire [3:0]  mux_rx_valid;
    
    assign mux_tx_data[7:0] = dbg_tx_data;
    assign mux_tx_valid[0]  = dbg_tx_valid & !mux_tx_busy[0];
    assign dbg_tx_busy      = mux_tx_busy[0];
    assign dbg_rx_data      = mux_rx_data[7:0];
    assign dbg_rx_valid     = mux_rx_valid[0];
    
    assign mux_tx_data[15:8] = term_tx_data;
    assign mux_tx_valid[1]  = term_tx_valid;
    assign term_tx_busy     = mux_tx_busy[1];
    assign term_rx_data     = mux_rx_data[15:8];
    assign term_rx_valid    = mux_rx_valid[1];
    
    assign mux_tx_data[23:16] = pc11_tx_data;
    assign mux_tx_valid[2]   = pc11_tx_valid;
    assign pc11_tx_busy     = mux_tx_busy[2];
    assign pc11_rx_data     = mux_rx_data[23:16];
    assign pc11_rx_valid    = mux_rx_valid[2];

    assign mux_tx_data[31:24] = rk11_tx_data;
    assign mux_tx_valid[3]   = rk11_tx_valid;
    assign rk11_tx_busy     = mux_tx_busy[3];
    assign rk11_rx_data     = mux_rx_data[31:24];
    assign rk11_rx_valid    = mux_rx_valid[3];

    uart_mux_top #(.NUM_PORTS(4)) DUT (.clock(clk),
                                       .reset(sys1801vm2_reset),
                                                
                                        // -- Interface to clients
                                        .txdata  (mux_tx_data  ),
                                        .tx_valid(mux_tx_valid),
                                        .tx_busy (mux_tx_busy ),
                                        .rxdata  (mux_rx_data  ),
                                        .rx_valid(mux_rx_valid),
                                        
                                        // -- Interface to actual UART
                                        .uart_txdata  (uart_tx_data ),
                                        .uart_tx_valid(uart_tx_valid),
                                        .uart_tx_busy (uart_tx_busy ),
                                        .uart_rxdata  (uart_rx_data ),
                                        .uart_rx_valid(uart_rx_valid)
                                        );


    uart_top dbg_uart (.clock(clk),.reset(sys1801vm2_reset),.ser_in(rx),.ser_out(tx),
                       .tx_data(uart_tx_data),.new_tx_data(uart_tx_valid),.tx_busy(uart_tx_busy),
                       .rx_data(uart_rx_data),.new_rx_data(uart_rx_valid),
                       .baud_freq(16'd48),.baud_limit(16'd577));  // 115200
                       //.baud_freq(16'd4),.baud_limit(16'd621));  // 9600
                       //.baud_freq(16'd1),.baud_limit(16'd624));  // 2400
    
    // -----------------------------------------------------------------
    // Secondary memory for startup vector
    
    reg secondary_read_rply, secondary_write_rply;
    reg [15:0] startup_vector;
    reg [15:0] startup_psw;
    reg [15:0] trap_vector;
    reg [15:0] trap_psw;
    reg [15:0] halt_vector;
    reg [15:0] halt_psw;
    
    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            secondary_read_rply   <= 1'b0;
            secondary_write_rply  <= 1'b0;
            secondary_readdata    <= 16'b0;
            startup_vector        <= STARTUP_VECTOR;
            trap_vector           <= STARTUP_VECTOR;
            halt_vector           <= STARTUP_VECTOR;
            startup_psw           <= STARTUP_PSW;
            trap_psw              <= STARTUP_PSW;
            halt_psw              <= STARTUP_PSW;
        end else begin
            secondary_read_rply   <= secondary_read_rply  & qbus_din;
            secondary_write_rply  <= secondary_write_rply & qbus_dout;
            if (qbus_read) begin
                secondary_read_rply  <= qbus_secondary;
                case(qbus_address)
                    16'o000000  : secondary_readdata <= startup_vector;
                    16'o000002  : secondary_readdata <= startup_psw;
                    16'o000004  : secondary_readdata <= trap_vector;           // TRAP PC
                    16'o000006  : secondary_readdata <= trap_psw;              // TRAP PSW
                    16'o000170  : secondary_readdata <= halt_vector;           // HALT PC
                    16'o000172  : secondary_readdata <= halt_psw;              // HALT PSW
                    default     : secondary_readdata <= startup_vector;        // HALT PC
                endcase
            end
            if (qbus_write & qbus_secondary) begin
                secondary_write_rply <= qbus_secondary;
                case(qbus_address)
                    16'o000000  : startup_vector <= qbus_writedata;
                    16'o000002  : startup_psw    <= qbus_writedata;
                    16'o000004  : trap_vector    <= qbus_writedata;           // TRAP PC
                    16'o000006  : trap_psw       <= qbus_writedata;           // TRAP PSW
                    16'o000170  : halt_vector    <= qbus_writedata;           // HALT PC
                    16'o000172  : halt_psw       <= qbus_writedata;           // HALT PSW
                    default     : startup_vector <= startup_vector;           // HALT PC
                endcase
            end
        end
    end
    
    
    // -----------------------------------------------------------------
    // RAM

    wire ram_addr_match;
`ifdef BRAM
    assign ram_addr_match = (qbus_address[15:12] < 4'h4);
    reg [15:0] primem [6143:0]; // 6 kW

    initial
        $readmemh("pri.mem",primem);

    reg primem_read_rply, primem_write_rply;
    reg [15:0] primem_readdata;
    
    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            primem_read_rply                        <= 1'b0;
            primem_write_rply                       <= 1'b0;
        end else begin
            primem_read_rply                        <= primem_read_rply  & qbus_din;
            primem_write_rply                       <= primem_write_rply & qbus_dout;
            if (ram_addr_match & qbus_read) begin
                primem_read_rply                    <= 1'b1;
                primem_readdata                     <= primem[{1'b0,qbus_address[15:1]}];
            end
            if (ram_addr_match & qbus_write) begin
            
                if (qbus_be & qbus_address[0])
                    primem[{1'b0,qbus_address[15:1]}][15:8] <= qbus_writedata[15:8];
                else if (qbus_be & ~qbus_address[0])
                    primem[{1'b0,qbus_address[15:1]}][7:0]  <= qbus_writedata[7:0];
                else
                    primem[{1'b0,qbus_address[15:1]}] <= qbus_writedata[15:0];
            
                primem_write_rply                   <= 1'b1;
            end
        end
    end
`else
    wire primem_read_rply, primem_write_rply;
    wire primem_rply;
    wire [15:0] primem_readdata;
    wire mem_busy;
    
    assign primem_read_rply  = primem_rply;
    assign primem_write_rply = primem_rply;
    
    qbus_23lc1024 spiram (
        .clock(clk),
        .reset(sys1801vm2_reset),
        // -- bus
        .write(qbus_write),
        .read(qbus_read),
        .din(qbus_din),
        .dout(qbus_dout),
        .address(qbus_address),
        .write_byte(qbus_be),
        .wdata(qbus_writedata),
        .rdata(primem_readdata),
        .rply(primem_rply),
        .ready(ready),
        .addr_match(ram_addr_match),
        // -- I/O
        .sio_i(sio_i),
        .sio_o(sio_o),
        .sio_oen(sio_oen),
        .cs_n(cs_n),
        .sck(sck));
`endif

    // -----------------------------------------------------------------
    // ODT ROM

    wire odt_hi_addr_match;
    wire odt_hi_read_rply, odt_hi_write_rply;
    wire [15:0] odt_hi_readdata;
    
    wire odt_lo_addr_match;
    wire odt_lo_read_rply, odt_lo_write_rply;
    wire [15:0] odt_lo_readdata;
    
    wire odt_my_lo_addr_match;
    wire odt_my_lo_read_rply, odt_my_lo_write_rply;
    wire [15:0] odt_my_lo_readdata;

    ODT #(.BASE_ADDRESS(16'o163000),
          .TOP_ADDRESS (16'o164777),
          .INIT_FILE   ("odt-my-hi.mem")) ODT_my_lo (
        .clk(clk),
        .reset(sys1801vm2_reset),
    // -- bus
        .read(qbus_read),
        .write(qbus_write),
        .din(din),
        .dout(dout),
        .address(qbus_address),
        .rdata(odt_my_lo_readdata),
        .read_rply(odt_my_lo_read_rply),
        .write_rply(odt_my_lo_write_rply),
        .addr_match(odt_my_lo_addr_match)
    );



    ODT #(.BASE_ADDRESS(16'o165000),
          .TOP_ADDRESS (16'o166777),
          .INIT_FILE   ("odt-lo.mem")) ODT_lo (
        .clk(clk),
        .reset(sys1801vm2_reset),
    // -- bus
        .read(qbus_read),
        .write(qbus_write),
        .din(din),
        .dout(dout),
        .address(qbus_address),
        .rdata(odt_lo_readdata),
        .read_rply(odt_lo_read_rply),
        .write_rply(odt_lo_write_rply),
        .addr_match(odt_lo_addr_match)
    );


    ODT #(.BASE_ADDRESS(16'o173000),
          .TOP_ADDRESS (16'o174777),
          .INIT_FILE   ("odt-hi.mem")) ODT_hi (
        .clk(clk),
        .reset(sys1801vm2_reset),
    // -- bus
        .read(qbus_read),
        .write(qbus_write),
        .din(din),
        .dout(dout),
        .address(qbus_address),
        .rdata(odt_hi_readdata),
        .read_rply(odt_hi_read_rply),
        .write_rply(odt_hi_write_rply),
        .addr_match(odt_hi_addr_match)
    );

    // -----------------------------------------------------------------
    // GPIO
    localparam GPIO_ADDRESS = 16'hFEEC;
    wire gpio_addr_match;
    reg [15:0] gpio;
    reg [15:0] gpio_readdata;
    reg gpio_read_rply, gpio_write_rply;
    
    assign gpio_addr_match = qbus_address[15:1] == GPIO_ADDRESS[15:1];
    
    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            gpio_read_rply   <= 1'b0;
            gpio_write_rply  <= 1'b0;
            gpio            <= 16'b0;
            gpio_readdata   <= 16'b0;
        end else begin
            gpio_read_rply          <= gpio_read_rply  & qbus_din;
            gpio_write_rply         <= gpio_write_rply & qbus_dout;
            if (gpio_addr_match & qbus_read) begin
                gpio_read_rply     <= 1'b1;
                gpio_readdata      <= gpio;
            end
            if (gpio_addr_match & qbus_write) begin
                if (qbus_be & qbus_address[0])
                    gpio[15:8] <= qbus_writedata[15:8];
                else if (qbus_be & ~qbus_address[0])
                    gpio[7:0]  <= qbus_writedata[7:0];
                else
                    gpio[15:0] <= qbus_writedata[15:0];
                gpio_write_rply    <= 1'b1;
                
            end
        end
    end
    
    
    // -----------------------------------------------------------------
    // SWITCH Reg
    localparam SWITCH_ADDRESS = 16'o177570;
    wire switch_addr_match;
    reg [15:0] switch;
    reg [15:0] switch_readdata;
    reg switch_read_rply, switch_write_rply;
    
    assign switch_addr_match = qbus_address[15:1] == SWITCH_ADDRESS[15:1];
    
    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            switch_read_rply   <= 1'b0;
            switch_write_rply  <= 1'b0;
            switch            <= 16'b0;
            switch_readdata   <= 16'b0;
        end else begin
            switch_read_rply          <= switch_read_rply  & qbus_din;
            switch_write_rply         <= switch_write_rply & qbus_dout;
            if (switch_addr_match & qbus_read) begin
                switch_read_rply     <= 1'b1;
                switch_readdata      <= switch;
            end
            if (switch_addr_match & qbus_write) begin
                if (qbus_be & qbus_address[0])
                    switch[15:8] <= qbus_writedata[15:8];
                else if (qbus_be & ~qbus_address[0])
                    switch[7:0]  <= qbus_writedata[7:0];
                else
                    switch[15:0] <= qbus_writedata[15:0];
                switch_write_rply    <= 1'b1;
            end
        end
    end
    
    // -----------------------------------------------------------------
    // PC & PSW SAVE location

    localparam SAVE_ADDRESS = 16'hFF80;
    wire psw_addr_match;
    reg [15:0] psw [128:0];
    reg [15:0] psw_readdata;
    reg psw_read_rply, psw_write_rply;
    
    assign psw_addr_match = qbus_address[15:7] == SAVE_ADDRESS[15:7];
    
    initial begin
        psw[8'd39] = 16'o163000; // for sim only
    end
    
    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            psw_read_rply   <= 1'b0;
            psw_write_rply  <= 1'b0;
            psw_readdata    <= 16'b0;
        end else begin
            psw_read_rply          <= psw_read_rply  & qbus_din;
            psw_write_rply         <= psw_write_rply & qbus_dout;
            if (psw_addr_match & qbus_read) begin
                psw_read_rply     <= 1'b1;
                psw_readdata      <= psw[qbus_address[6:1]];
            end
            if (psw_addr_match & qbus_write) begin
                if (qbus_be & qbus_address[0])
                    psw[qbus_address[6:1]][15:8] <= qbus_writedata[15:8];
                else if (qbus_be & ~qbus_address[0])
                    psw[qbus_address[6:1]][7:0]  <= qbus_writedata[7:0];
                else
                    psw[qbus_address[6:1]][15:0] <= qbus_writedata[15:0];
                psw_write_rply    <= 1'b1;
            end
        end
    end
    
    
    // -----------------------------------------------------------------
    // DL11 Serial Port
    
    wire terminal_addr_match;
    wire terminal_rply;
    wire [15:0] terminal_readdata;
    wire [15:0] terminal_interrupt_vector;
    wire        terminal_virq;
    wire        terminal_iako_rply;    
    wire        dl11_iako;

    dl11b dl11b_terminal (
        .clock(clk),
        .reset(sys1801vm2_reset),
        .write(qbus_write),
        .read(qbus_read),
        .din(qbus_din),
        .dout(qbus_dout),
        .address(qbus_address),
        .write_byte(qbus_wtbt),
        .wdata(qbus_writedata),
        .rdata(terminal_readdata),
        .rply(terminal_rply),
        .ready(),
        .addr_match(terminal_addr_match),
        .irq(terminal_virq),
        .irq_vector(terminal_interrupt_vector),
        .iako_rply(terminal_iako_rply),
        .iako(dl11_iako),
        .tx_data(term_tx_data),
        .tx_valid(term_tx_valid),
        .tx_busy(term_tx_busy),
        .rx_data(term_rx_data),
        .rx_valid(term_rx_valid)
    );
    
    // -----------------------------------------------------------------
    // PC11 Paper Tape Reader
    
    wire pc11_addr_match;
    wire pc11_rply;
    wire [15:0] pc11_readdata;
    wire [15:0] pc11_interrupt_vector;
    wire        pc11_virq;
    wire        pc11_iako_rply;
    wire        ready;
    wire        pc11_iako;
    

    dl11b #(.BASE_ADDRESS(16'o177550),
            .RCVR_INT_VECTOR(16'o70),
            .XMIT_INT_VECTOR(16'o74),
            .PC11(1)) dl11b_pc11 (
        .clock(clk),
        .reset(sys1801vm2_reset),
        .write(qbus_write),
        .read(qbus_read),
        .din(qbus_din),
        .dout(qbus_dout),
        .address(qbus_address),
        .write_byte(qbus_wtbt),
        .wdata(qbus_writedata),
        .rdata(pc11_readdata),
        .rply(pc11_rply),
        .ready(),
        .addr_match(pc11_addr_match),
        .irq(pc11_virq),
        .irq_vector(pc11_interrupt_vector),
        .iako_rply(pc11_iako_rply),
        .iako(pc11_iako),
        .tx_data(pc11_tx_data),
        .tx_valid(pc11_tx_valid),
        .tx_busy(pc11_tx_busy),
        .rx_data(pc11_rx_data),
        .rx_valid(pc11_rx_valid)
    );
    
    // -----------------------------------------------------------------
    // RK11 disk controller


    wire  [15:0]  rk11_readdata;
    wire          rk11_rply;
    wire          rk11_addr_match;
    
    // -- qbus master
    wire         rk11_dmr;
    wire         rk11_sack;
    wire [15:0]  rk11_ad;
    wire [15:0]  rk11_rdata;
    wire         rk11_dout;
    wire         rk11_din;
    wire         rk11_wtbt;
    wire         rk11_sync;
    
    // -- interrupt
    wire         rk11_irq;
    wire [15:0]  rk11_irq_vector;
    wire         rk11_iako_rply;
    wire         rk11_iako;
    
    // -- Serial I/O
    wire [7:0]   rk11_tx_data;
    wire         rk11_tx_valid;
    wire         rk11_tx_busy;
    
    wire [7:0]   rk11_rx_data;
    wire         rk11_rx_valid;
    wire         rk11_sel = 1'b0;
    
    wire [7:0]   rk11_debug;

    rk11 rk_inst (
    .clock                (clk),
    .reset                (sys1801vm2_reset),
    .ce                   (ce_clci),
    // -- qbus slave
    .slave_write          (qbus_write),
    .slave_read           (qbus_read),
    .slave_write_byte     (qbus_be),
    .slave_din            (qbus_din),
    .slave_dout           (qbus_dout),
    .slave_address        (qbus_address),
    .slave_wdata          (qbus_writedata),
    .slave_rdata          (rk11_readdata),
    .slave_rply           (rk11_rply),
    .slave_addr_match     (rk11_addr_match),
    
    // -- qbus master
    .master_dmr           (rk11_dmr),
    .master_dmgo          (dmgo),
    .master_sack          (rk11_sack),
    .master_ad            (rk11_ad),
    .master_rdata         (qbus_readdata),
    .master_dout          (rk11_dout),
    .master_din           (rk11_din),
    .master_wtbt          (rk11_wtbt),
    .master_sync          (rk11_sync),
    .master_ar            (ar),
    .master_rply          (rply),
    
    // -- interrupt
    .irq                  (rk11_irq),
    .irq_vector           (rk11_irq_vector),
    .iako_rply            (rk11_iako_rply),
    .iako                 (qbus_iako),
    
    // -- Serial I/O
    .tx_data              (rk11_tx_data),
    .tx_valid             (rk11_tx_valid),
    .tx_busy              (rk11_tx_busy),
                         
    .rx_data              (rk11_rx_data),
    .rx_valid             (rk11_rx_valid),
    .debug                (rk11_debug)
    
    );
    
    // -----------------------------------------------------------------
    // uSynth

    wire usynth_addr_match;
    wire usynth_rply;

    qbus_usynth qbus_usynth_i (
        .clock(clk),
        .reset(1'b1),
        // -- bus
        .write(qbus_write),
        .read(qbus_read),
        .write_byte(qbus_wtbt),
        .din(qbus_din),
        .dout(qbus_dout),
        .address(qbus_address),
        .wdata(qbus_writedata),
        .rdata(),
        .rply(usynth_rply),
        .addr_match(usynth_addr_match),
        // -- I/O
        .pwm(pwm)
    );
    
    // -----------------------------------------------------------------
    // KW11 Line Clock
    
    wire kw11_addr_match;
    wire kw11_rply;
    wire [15:0] kw11_readdata;
    wire kw11_irq;
    wire kw11_iako_rply;
    wire [15:0] kw11_irq_vector;
    wire kw11_iako;
    
    kw11 kw11_inst (
        .clock(clk),
        .reset(sys1801vm2_reset),
        .ce50hz(ce_50hz),
        .write(qbus_write),
        .read(qbus_read),
        .dout(qbus_dout),
        .din(qbus_din),
        .address(qbus_address),
        .wdata(qbus_writedata),
        .rdata(kw11_readdata),
        .rply(kw11_rply),
        .addr_match(kw11_addr_match),
        .interrupt_vector(kw11_irq_vector),
        .irq(kw11_irq),
        .iako(kw11_iako),
        .iako_rply(kw11_iako_rply)
    );


    // -----------------------------------------------------------------
    // Readdata Multiplexer
    
    wire addr_match;
    assign addr_match = ram_addr_match      | odt_lo_addr_match | odt_hi_addr_match | odt_my_lo_addr_match |
                        switch_addr_match   | gpio_addr_match   | pc11_addr_match   | rk11_addr_match      |
                        terminal_addr_match | psw_addr_match    | usynth_addr_match | kw11_addr_match;
    
    always @(*) begin
        if (ram_addr_match) begin
            primary_readdata <= primem_readdata;
        end else if (odt_my_lo_addr_match) begin
            primary_readdata <= odt_my_lo_readdata;
        end else if (odt_lo_addr_match) begin
            primary_readdata <= odt_lo_readdata;
        end else if (odt_hi_addr_match) begin
            primary_readdata <= odt_hi_readdata;
        end else if (kw11_addr_match) begin
            primary_readdata <= kw11_readdata;
        end else if (switch_addr_match) begin
            primary_readdata <= switch_readdata;
        end else if (gpio_addr_match) begin
            primary_readdata <= gpio_readdata;
        end else if (rk11_addr_match) begin
            primary_readdata <= rk11_readdata;
        end else if (pc11_addr_match) begin
            primary_readdata <= pc11_readdata;
        end else if (terminal_addr_match) begin
            primary_readdata <= terminal_readdata;
        end else if (psw_addr_match) begin
            primary_readdata <= psw_readdata;
        end else begin
            primary_readdata <= 16'b0;
        end
    end
    
    reg rk11_iako_en;
    reg kw11_iako_en;
    reg dl11_iako_en;
    reg pc11_iako_en;
    reg irq_in_progress; 
    
    assign rk11_iako = rk11_iako_en & qbus_iako;
    assign kw11_iako = kw11_iako_en & qbus_iako;
    assign dl11_iako = dl11_iako_en & qbus_iako;
    assign pc11_iako = pc11_iako_en & qbus_iako;
    
    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            rk11_iako_en          <= 1'b0;
            kw11_iako_en          <= 1'b0;
            dl11_iako_en          <= 1'b0;
            pc11_iako_en          <= 1'b0;
            qbus_interrupt_vector <= 16'b0;
        end else begin
            if (~irq_in_progress) begin
                rk11_iako_en <= 1'b0;
                kw11_iako_en <= 1'b0;
                dl11_iako_en <= 1'b0;
                pc11_iako_en <= 1'b0;
                if (terminal_virq) begin
                    dl11_iako_en          <= 1'b1;
                    qbus_interrupt_vector <= terminal_interrupt_vector;
                end else if (pc11_virq) begin
                    pc11_iako_en          <= 1'b1;
                    qbus_interrupt_vector <= pc11_virq;
                end else if (rk11_irq) begin
                    rk11_iako_en          <= 1'b1;
                    qbus_interrupt_vector <= rk11_irq_vector;
                end else if (kw11_irq) begin
                    kw11_iako_en          <= 1'b1;
                    qbus_interrupt_vector <= kw11_irq_vector;
                end else begin
                    qbus_interrupt_vector <= 16'b0;
                end
            end
        end
    end
    
    // -----------------------------------------------------------------
    // QBUS Backbone

    wire qbus_dout;
    wire qbus_din;
    wire qbus_sync;
    wire qbus_sel;
    wire qbus_wtbt;
    wire [15:0] qbus_ad;
    reg  qbus_sack_dly;
    
    //
    // -- DMA (3 Channels + CPU for now)
    //
    
    wire rl_dout      = 1'b0;
    wire rl_din       = 1'b0;
    wire rl_sync      = 1'b0;
    wire rl_sel       = 1'b0;
    wire rl_wtbt      = 1'b0;
    wire [15:0] rl_ad = 16'b0;
    wire rl_sack      = 1'b0;
    wire rl_dmr       = 1'b0;
    
    wire aux_dout     ;
    wire aux_din      ;
    wire aux_sync     ;
    wire aux_sel = 1'b1;
    wire aux_wtbt     ;
    wire [15:0] aux_ad ;
    wire aux_sack     ;
    wire aux_dmr      ;
    
    dma_led dma_led_i (.clock(clk),
                       .reset(1'b1),
                       .ce(ce_clci),
                       .ce_50hz(ce_50hz),
                       .dmr(aux_dmr),
                       .dmgo(dmgo),
                       .sack(aux_sack),
                       .writedata(aux_ad),
                       .readdata(qbus_readdata),
                       .dout(aux_dout),
                       .din(aux_din),
                       .wtbt(aux_wtbt),
                       .sync(aux_sync),
                       .ar(ar),
                       .rply(rply));

    // Multiplex QBUS signals
    
    reg [3:0] qbus_mst_sel;
    
    assign qbus_dout = (qbus_mst_sel[0]) ? dout       :
                       (qbus_mst_sel[1]) ? rk11_dout  :
                       (qbus_mst_sel[2]) ? rl_dout  :
                       (qbus_mst_sel[3]) ? aux_dout : 1'b0;

    assign qbus_din =  (qbus_mst_sel[0]) ? din       :
                       (qbus_mst_sel[1]) ? rk11_din  :
                       (qbus_mst_sel[2]) ? rl_din  :
                       (qbus_mst_sel[3]) ? aux_din : 1'b0;

    assign qbus_sync = (qbus_mst_sel[0]) ?     sync   :
                       (qbus_mst_sel[1]) ?  rk11_sync :
                       (qbus_mst_sel[2]) ?  rl_sync :
                       (qbus_mst_sel[3]) ? aux_sync : 1'b0;

    assign qbus_sel  = (qbus_mst_sel[0]) ?     sel   :
                       (qbus_mst_sel[1]) ?  rk11_sel :
                       (qbus_mst_sel[2]) ?  rl_sel :
                       (qbus_mst_sel[3]) ? aux_sel : 1'b0;
                       
    assign qbus_wtbt = (qbus_mst_sel[0]) ?     wtbt   :
                       (qbus_mst_sel[1]) ?  rk11_wtbt :
                       (qbus_mst_sel[2]) ?  rl_wtbt :
                       (qbus_mst_sel[3]) ? aux_wtbt : 1'b0;
                       
    assign qbus_ad   = (qbus_mst_sel[0]) ?     ad_i   :
                       (qbus_mst_sel[1]) ?  rk11_ad   :
                       (qbus_mst_sel[2]) ?  rl_ad   :
                       (qbus_mst_sel[3]) ? aux_ad   : 1'b0;
    
    reg [3:0] sel_posedge;
    reg [3:0] din_posedge;
    reg [3:0] dout_posedge;
    reg [3:0] sync_posedge;
    reg  dmgo_posedge;
    reg sync_dly;
    reg dmgo_dly;
    
    localparam QBUS_WAIT_TRANSFER      = 16'h0001,
               QBUS_READ_ADDRESSLESS   = 16'h0002,
               QBUS_ADDR               = 16'h0004,
               QBUS_WAIT_TRANSFER_TYPE = 16'h0008,
               QBUS_WRITE              = 16'h0010,
               QBUS_WRITE_WAIT         = 16'h0020,
               QBUS_READ               = 16'h0040,
               QBUS_READ_WAIT          = 16'h0080,
               QBUS_INTERRUPT          = 16'h0100,
               QBUS_INTERRUPT_WAIT     = 16'h0200,
               QBUS_WAIT_SACK          = 16'h0400;
              
    localparam INITIAL_VECTOR          = 16'o0000,
               STARTUP_VECTOR          = 16'o173000, // ODT, otherwise 16'o2000 for Word 512, Byte 1024
               STARTUP_PSW             = 16'o0000; 

    reg [15:0]  qbus_state;
    reg [15:0] qbus_address;
    reg [15:0] qbus_writedata;
    reg qbus_write;
    reg qbus_read;
    reg qbus_be;
    reg dbg_ce_dly;
    reg qbus_secondary;
    wire [15:0] qbus_readdata;
    reg [15:0] qbus_interrupt_vector;
    reg  qbus_iako;
    
    wire qbus_read_rply;
    wire qbus_write_rply;
    wire qbus_iako_rply;

    reg [15:0]  primary_readdata;
    reg [15:0] secondary_readdata;

    assign qbus_readdata    = (secondary_read_rply) ? secondary_readdata : primary_readdata;

    assign qbus_read_rply  = kw11_rply | rk11_rply | usynth_rply | odt_my_lo_read_rply   | odt_lo_read_rply   | odt_hi_read_rply  | pc11_rply | terminal_rply | secondary_read_rply  | switch_read_rply  | gpio_read_rply  | primem_read_rply | psw_read_rply;
    assign qbus_write_rply = kw11_rply | rk11_rply | usynth_rply | odt_my_lo_write_rply  | odt_lo_write_rply  | odt_hi_write_rply | pc11_rply | terminal_rply | secondary_write_rply | switch_write_rply | gpio_write_rply | primem_write_rply | psw_write_rply;
    assign qbus_iako_rply  = terminal_iako_rply | pc11_iako_rply | rk11_iako_rply | kw11_iako_rply;
    assign virq            = terminal_virq      | pc11_virq | rk11_irq | kw11_irq;

    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            qbus_state     <= QBUS_WAIT_TRANSFER;
            ad_oe          <= 1'b0;
            sel_dly        <= 1'b0;
            din_dly        <= 1'b0;
            dout_dly       <= 1'b0;
            sync_dly       <= 1'b0;
            dmgo_dly       <= 1'b0;
            sel_posedge    <= 4'b0;
            din_posedge    <= 4'b0;
            sync_posedge   <= 4'b0;
            dout_posedge   <= 4'b0;
            dmgo_posedge   <= 1'b0;
            ar             <= 1'b0;
            rply           <= 1'b0;
            sack           <= 1'b0;
            qbus_secondary <= 1'b0;
            qbus_writedata <= 16'b0;
            qbus_address   <= 16'b0;
            qbus_write     <= 1'b0;
            qbus_be        <= 1'b0;
            dbg_send       <= 1'b0;
            qbus_read      <= 1'b0;
            qbus_iako      <= 1'b0;
            dmr            <= 1'b0;
            qbus_mst_sel   <= 4'b0001;
            dbg_data       <= 32'b0;
            irq_in_progress <= 1'b0;
        end else begin
            dbg_send       <= 1'b0;
            qbus_write     <= 1'b0;
            qbus_read      <= 1'b0;
            qbus_iako      <= iako;
            ad_oe          <= ad_oe & qbus_din;
            rply           <= qbus_read_rply | qbus_write_rply | qbus_iako_rply;
            sack           <= rk11_sack | rl_sack | aux_sack;
            dmr            <= rk11_dmr  | rl_dmr  | aux_dmr;

            if (qbus_sack_dly & ~sack) begin
                qbus_mst_sel   <= 4'b0001;
            end
            
            if (virq) begin
                irq_in_progress <= 1'b1;
            end
            
            if (ce_clci) begin
                sel_dly       <= qbus_sel;
                din_dly       <= qbus_din;
                dout_dly      <= qbus_dout;
                sync_dly      <= qbus_sync;
                dmgo_dly      <= dmgo;
                qbus_sack_dly <= sack;
                ad_oe         <= 1'b0;
                
                if (qbus_sack_dly ^ sack) begin
                    dout_posedge  <= 4'b0;
                    din_posedge   <= 4'b0;
                    sel_posedge   <= 4'b0;
                    sync_posedge  <= 1'b0;
                end else begin
                    dout_posedge  <= {dout_posedge[2:0],(~dout_dly & qbus_dout)};
                    din_posedge   <= {din_posedge[2:0],(~din_dly & qbus_din)};
                    sel_posedge   <= {sel_posedge[2:0],(~sel_dly & qbus_sel)};
                    sync_posedge  <= {sync_posedge[2:0],(~sync_dly & qbus_sync)};
                end
                
                dmgo_posedge  <= (~dmgo_dly & dmgo) | dmgo_posedge;

                case (qbus_state)
                    QBUS_WAIT_TRANSFER : begin
                        qbus_be      <= 1'b0;
                        ar           <= 1'b0;
                        if (din_posedge[1] & qbus_sel) begin
                            qbus_state     <= QBUS_READ_ADDRESSLESS;
                            ad_o           <= INITIAL_VECTOR;
                            ad_oe          <= 1'b1;
                        end else if (din_posedge[1] & iako) begin
                            qbus_state     <= QBUS_INTERRUPT_WAIT;
                            dbg_send       <= 1'b1;
                            dbg_data       <= {"IAKO"};
                        end else if (sync_posedge[0]) begin
                            qbus_state     <= QBUS_ADDR;
                            qbus_secondary <= qbus_sel;
                        end else if (dmgo_posedge) begin
                            qbus_state     <= QBUS_WAIT_SACK;
                            dmgo_posedge   <= 1'b0;
                            dbg_send       <= 1'b1;
                            dbg_data       <= {"DMGO"};
                        end
                    end
                    QBUS_READ_ADDRESSLESS : begin
                        if (qbus_din == 1'b0 && qbus_sel == 1'b0) begin
                            ad_oe        <= 1'b0;
                            qbus_state   <= QBUS_WAIT_TRANSFER;
                        end
                    end
                    QBUS_ADDR : begin
                        ar               <= 1'b1;
                        qbus_address     <= qbus_ad;
                        qbus_state       <= QBUS_WAIT_TRANSFER_TYPE;
                    end
                    QBUS_WAIT_TRANSFER_TYPE : begin
                        if (din_posedge[1]) begin
                            qbus_state       <= QBUS_READ;
                            dbg_send         <= 1'b1;
                            dbg_data         <= {"RA",qbus_address};
                        end else if (dout_posedge[1]) begin
                            qbus_state       <= QBUS_WRITE;
                            qbus_be          <= qbus_wtbt;
                            dbg_send         <= 1'b1;
                            dbg_data         <= {"W",qbus_wtbt?"B":"W",qbus_address};
                        end
                    end
                    QBUS_READ   : begin
                        qbus_read        <= 1'b1;
                        qbus_state       <= QBUS_READ_WAIT;
                    end
                    QBUS_READ_WAIT : begin
                        if (!sack) begin
                            ad_o         <= qbus_readdata;
                            ad_oe        <= qbus_din;
                        end
                        
                        if (sync == 1'b0) begin
                            // We're ready for the next cycle
                            qbus_state       <= QBUS_WAIT_TRANSFER;
                            dbg_send         <= 1'b1;
                            dbg_data         <= {"r:",qbus_readdata};
                        end else if (dout_posedge[1]) begin
                            // If sync is kept low and dout arrives, this is a RMW operation (KM1801VM2-1.djvu "pg 49" -> 51)
                            qbus_state       <= QBUS_WRITE;
                            qbus_be          <= qbus_wtbt;
                            dbg_send         <= 1'b1;
                            dbg_data         <= {(qbus_wtbt)?"MB":"MW", qbus_address};
                        end
                    end
                    QBUS_WRITE : begin
                        qbus_writedata <= qbus_ad;
                        qbus_write     <= 1'b1;
                        qbus_state     <= QBUS_WRITE_WAIT;
                        dbg_send       <= 1'b1;
                        dbg_data       <= {"w:",qbus_ad};
                    end
                    QBUS_WRITE_WAIT : begin
                        if (qbus_dout == 1'b0) begin
                            qbus_state       <= QBUS_WAIT_TRANSFER;
                        end
                    end
                    QBUS_INTERRUPT : begin
                        qbus_state <= QBUS_INTERRUPT_WAIT;
                    end
                    QBUS_INTERRUPT_WAIT : begin
                        ad_o  <= qbus_interrupt_vector;
                        ad_oe <= 1'b1;
                        if (qbus_din == 1'b0) begin
                            qbus_state <= QBUS_WAIT_TRANSFER;
                            irq_in_progress <= 1'b0;
                        end
                    end
                    QBUS_WAIT_SACK: begin
                        if (sack == 1'b1) begin
                            dbg_send       <= 1'b1;
                            dbg_data       <= {"DMA",5'b0,aux_sack,rl_sack,rk11_sack};
                        
                        
                            if (rk11_sack) begin
                                qbus_mst_sel <= 4'b0010;
                            end else if (rl_sack) begin
                                qbus_mst_sel <= 4'b0100;
                            end else begin
                                qbus_mst_sel <= 4'b1000;
                            end
                            qbus_state   <= QBUS_WAIT_TRANSFER;
                        end
                    end
                endcase
            end
        end
    end

    //
    // -- DBG hardware
    //

    integer i;
    reg [31:0] dbg_data;
    reg [31:0] dbg_txreg;
    reg [2:0]  dbg_txcnt;
    reg dbg_uart_send;
    reg [3:0] dbg_state;
    reg dbg_send;
    reg [31:0] dbg_trace_mem_out;

    reg [5:0] dbg_addr;
    reg [5:0] dbg_txaddr;
    reg [31:0] dbg_trace_mem [63:0];

    initial begin
        for (i = 0; i < 64; i=i+1)
            dbg_trace_mem[i] <= "XXXX";
    end

    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            dbg_addr <= 1'b0;
        end else begin
            if (dbg_send && dbg_state == DBG_STATE_IDLE) begin
                dbg_trace_mem[dbg_addr] <= dbg_data;
                dbg_addr <= dbg_addr + 1;
            end
            dbg_trace_mem_out <= dbg_trace_mem[dbg_txaddr];
        end
    end

    // QBUS Debug Monitor

    localparam DBG_STATE_IDLE       = 4'b0001,
               DBG_STATE_SEND_TRACE = 4'b0010,
               DBG_STATE_SEND       = 4'b0100,
               DBG_STATE_SHIFT      = 4'b1000;

    assign dbg_tx_data  = dbg_txreg[31:24];
    assign dbg_tx_valid = dbg_uart_send;

    always @(posedge clk) begin
        if (sys1801vm2_reset) begin
            dbg_txreg <= 32'b0;
            dbg_txcnt <= 3'b0;
            dbg_uart_send <= 1'b0;
            dbg_state <= DBG_STATE_IDLE;
            dbg_txaddr  <= 4'b0;
            halt <= 1'b0;
            
        end else begin
            if (ce_clci) begin
                halt <= 1'b0;
            end
            dbg_uart_send <= 1'b0;
            case (dbg_state)
                DBG_STATE_IDLE : begin
                    if (dbg_rx_valid) begin
                        if (dbg_rx_data == 8'h48) begin // 'H'
                            halt <= 1'b1;
                        end
                        dbg_state  <= DBG_STATE_SEND_TRACE;
                        dbg_txaddr <= dbg_addr + 1;
                    end
                end
                DBG_STATE_SEND_TRACE : begin
                    if (dbg_txaddr == dbg_addr) begin
                        dbg_state <= DBG_STATE_IDLE;
                    end else begin
                        dbg_txcnt <= 3'b100;
                        dbg_txreg <= dbg_trace_mem_out;
                        dbg_state <= DBG_STATE_SEND;
                    end
                end
                DBG_STATE_SEND : begin
                    if (dbg_tx_busy == 1'b0) begin
                        dbg_uart_send <= 1'b1;
                        dbg_txcnt     <= dbg_txcnt - 1;
                        dbg_state     <= DBG_STATE_SHIFT;
                    end
                end
                DBG_STATE_SHIFT : begin
                    dbg_txreg <= {dbg_txreg[23:0],8'b0};
                    if (dbg_txcnt == 3'b0) begin
                        dbg_state <= DBG_STATE_SEND_TRACE;
                        dbg_txaddr  <= dbg_txaddr+1;
                    end else begin
                        dbg_state <= DBG_STATE_SEND;
                    end
                end
            endcase
        end
    end

    assign dbg = {dmgo,rk11_debug[4:0]};

endmodule

