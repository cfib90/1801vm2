# 1801vm2+FPGA system #

The [1801vm2](https://en.wikipedia.org/wiki/1801_series_CPU#K1801VM2) is a Soviet implementation of a QBUS LSI-11 CPU.
It is binary compatible with the PDP-11.

In this project, I try to implement a backbone and peripherals for the 1801vm2
in order to implement a PDP-11-like system.

# Current Status #

* With some modding, the KiCad PCB actually works.
* The FPGA implements the necessary bus FSM, RAM, a debug GPIO, and a DL11 "Asynchronous Line Interface" i.e., serial port.
* Interrupts and RMW transfers are working.
* I'm able to run MACRO11 programs.

See also [this video](https://twitter.com/_stderr/status/939685943845548032)


Be aware that this project is very alpha, the code is in no way polished or even
verified very well...

# Project Structure #

    .
    ├── 1801vm1-implementation      ; Verilog implementation of the predecessor
    │   ├── 1801verilog             ; (external code)
    │   └── 1801vm1
    ├── docs                        ; Documents...
    │   ├── 1801vm2                 ; ... about the processor
    │   ├── iCE40                   ; ... about the FPGA board
    │   ├── Macro11                 ; ... about MACRO11 assembly
    │   ├── PCB                     ; ... about some PCB components
    │   └── PDP11-Periphs           ; ... about some PDP11 peripherals
    ├── hx8k                        ; FPGA implementation
    │   ├── gen                     ;   generated HDL modules
    │   ├── impl                    ;   Yosys+Icestorm implementation directory
    │   ├── impl_board_test         ;   Board test design
    │   ├── rtl                     ;   HDL sources
    │   └── tools                   ;   FPGA implementation specific tools
    ├── kicad                       ; Adapter board (Level Shifters mostly) for the HX8K breakout board
    ├── macro11                     ; Macro11 code
    └── tools                       ; Software build tools
        └── MACRO11                 ; Subproject: MACRO11 cross assembler


# Current Project Setup #

Currently, the 1801vm2 sits in an adapter PCB for the HX8K breakout board.
The main functionality of this board is to provide bidirectional 5V <-> 3V3 
level shifting.

The FPGA design implements a backplane (bus multiplexer, translation of bus commands)
and some peripherals. Currently, 6KiW of RAM is emulated.

Code is compiled using a cross-assembler and loaded as memory initialization file
for the main memory for the 1801vm2.

# External Code #

* The UART I use (`hx8k/rtl/uart2bus/`) comes from the [UART2BUS project](https://github.com/freecores/uart2bus)
* The MACRO11 assembler (`tools/MACRO11`) is forked from https://github.com/j-hoppe/MACRO11
* The Verilog implementation of the 1801vm1 under `1801vm1-implementation`
  was useful for understanding some basic bus concepts. It is from http://u.zeptobars.ru/yuot/1801/VM1/vm1_rev12j.rar
  Some time, I plan to implement a proper testbench system for the FPGA backbone.
  I might use the 1801vm1 for this purpose.

# Licenses #

* Hardware components (HDL sources in `hx8k` with the exception of the UART, and
  the PCB) are licensed under the Cern OHL. See HARDWARE.LICENSE.

* Software components (basically various small scripts and the assembly programs
  for the 1801vm2) are licensed under the GNU GPLv2. See SOFTWARE.LICENSE.

