#!/usr/bin/perl

#
# Perl script that parses the MACRO11 listing format to generate a mem file
# There might be cleaner ways to do this (objdump of some sorts is included
# in the MACRO11 package) but this works for me for now.
#

use String::Trim 'trim';
use POSIX 'ceil';

my $RAMSIZE=6144;

my @ram  = ((0)x$RAMSIZE);
my $addr = 0;
my $dat1;
my $dat2;

while(<>) {
    if ($_ =~ /^\s+([0-9]+ )?([0-9]{6})(( {4}[0-9]{3} )+)/) {
        my @bytes = split(/\s+/,trim($3));
        for (my $a = 0; $a < scalar(@bytes); $a++) {
            my $byte = oct(@bytes[$a]);
            my $x    = "";
            if ($addr & 1) {
                $ram[$addr/2] = ($byte << 8) | ($ram[$addr/2]&0xFF);
            } else {
                $ram[$addr/2] = ($ram[$addr/2]&0xFF00) | $byte;
            }
            $addr++;
        }
    } elsif ($_ =~ /^\s+([0-9]+) ([0-9]{6}) ([0-9]{6})([G ]+)([0-9]{6})?/) {
        $dat1 = $3;
        $dat2 = $5;
        if ($4 =~ /^G/) {
            $addr = oct($dat2)*2;
        } else {
            $ram[$addr/2] = oct($dat1);
            $addr+=2;
            if (defined $dat2) {
                $ram[$addr/2] = oct($dat2);
                $addr+=2;
            }
        }
    }
}

for (my $wa = 0; $wa < scalar(@ram); $wa++) {
    printf("%04x\n",@ram[$wa]);
}

