#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

void wait(int fd)
{
    char inb;
    if(read(fd,&inb,1) != 1) {
        perror("read() from PTY");
        exit(EXIT_FAILURE);
    }
    if (inb != 0x05) {
        fprintf(stderr,"Expected ENQ, got %02x\n",inb);
    }
}

enum load_state_type {
    LEAD_IN, SIGNATURE, LENGTH, LOAD_ADDR, DATA, CHKSUM
};

int txword(int fd, char w[2], int len) {
        for(int i = 0; i < len; i++) {
            wait(fd);
            write(fd,&w[i],1);
        }
        return 0;
}

int main(int argc, char *argv[])
{
    unsigned char w[2];
    int nw = 0;
    if (argc < 2) {
        fprintf(stderr,"Usage: %s DEVICE\n",argv[0]);
        return -1;
    }
    

    int fd = open(argv[1],O_RDWR | O_SYNC);

    /* Switch echo off etc */
    struct termios param;
    tcgetattr(fd, &param);
    cfmakeraw (&param);
    tcsetattr(fd, TCSANOW, &param);
    tcflush(fd,TCIOFLUSH);

    enum load_state_type state = LEAD_IN;
    int len;
    uint16_t bnum = 0;
    uint8_t chksum = 0;

    if (argc == 3 && !strcmp(argv[2],"DIRECT")) {
        while(1) {
            printf("Word %d\n",bnum++);
            if(fread(w,1,2,stdin) != 2) break;
            txword(fd,w,2);
        }
    } else {

        while(1) {
            uint16_t word;
            uint8_t  byte;
            if (state == LEAD_IN || state == SIGNATURE ||
                state == LENGTH  || state == LOAD_ADDR) {
                if(fread(w,1,2,stdin) != 2) break;
                word = (((uint16_t)w[1])<<8) | w[0];
                chksum += w[0] + w[1];
            } else {
                if(fread(w,1,1,stdin) != 1) break;
                byte = w[0];
                chksum += w[0];
            }
            switch (state) {
                case LEAD_IN:
                    if (word == 0) break;
                case SIGNATURE:
                    if (word == 0x0001) {
                        txword(fd,w,2);
                        state = LENGTH;
                        printf("Block %d\n",bnum++);
                        break;
                    } else {
                        printf("Tape end\n");
                        exit(EXIT_FAILURE);
                    }
                case LENGTH:
                    txword(fd,w,2);
                    len = (word-6);
                    printf("    Length: %d\n",len);
                    state = LOAD_ADDR;
                    break;
                case LOAD_ADDR:
                    txword(fd,w,2);
                    if (len == 0) {
                        if (word&1) { printf ("    HALT\n"); }
                        else { printf("    EXEC at 0%o\n",word); }
                        state = CHKSUM;
                    } else {
                        state = DATA;
                        printf("    LADDR: 0%o\n",word);
                    }
                    break;
                case DATA:
                    txword(fd,w,1);
                    len -= 1;
                    //printf("        %d bytes remaining\n",len);
                    if (len <= 0) {
                        state = CHKSUM;
                    }
                    break;
                case CHKSUM:
                    txword(fd,w,1);
                    printf("    CHECKSUM = %x (%x)\n",byte,chksum);
                    if (chksum) {
                        printf("    WARNING: Checksum is not 0\n");
                    }
                    chksum = 0;
                    if (len == -6) {
                        goto END;
                    }
                    state = SIGNATURE;
                    break;
            }
        }
    }
END:

    close(fd);

    return 0;
}
