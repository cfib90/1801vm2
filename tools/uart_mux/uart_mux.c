#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <sys/select.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>


#define MAX_CHANNELS 256
int channel_masters[MAX_CHANNELS] = {0};
int channel_slaves[MAX_CHANNELS] = {0};
int num_channels;
int serial = -1;

void terminate(int signal)
{
    (void) signal;
    for (int i = 0; i < num_channels; i++) {
        fprintf(stderr,"Closing: %d\n",close(channel_masters[i]));
        fprintf(stderr,"Closing: %d\n",close(channel_slaves[i]));
    }
    close(serial);
    exit(EXIT_SUCCESS);
}

int port_setup(int fd_dev, int baudrate)
{
    /**************************************************************************
     * Serial port configuration                                              *
     *************************************************************************/

    struct termios my_serport;
    /* we need to set some parameters for a serial port */
    /* for an explanation, see 'man 3 termios' */

    memset(&my_serport,0,sizeof(my_serport));
    cfsetospeed (&my_serport, baudrate);
    cfsetispeed (&my_serport, baudrate);

    /* The flags and fields in c_iflag control parameters usually associated with asynchronous serial data transmission. */
    /* If this bit is set, break conditions are ignored.
     * A break condition is defined in the context of asynchronous serial data
     * transmission as a series of zero-value bits longer than a single byte. */
    my_serport.c_iflag &= ~IGNBRK;

    /* The flags in c_lflag generally control higher-level aspects of input
       processing than the input modes flags in c_iflag
       such as echoing, signals, and the choice of canonical or noncanonical input.*/
    my_serport.c_lflag = 0;          /* no canonical processing: read() returns only after timeout, no local echo */

    /* The flags and fields in c_oflag control how output characters are
       translated and padded for display. */
    my_serport.c_oflag = 0;                /* no remapping, no delays */

    /* In noncanonical input mode, the special editing characters such as ERASE
       and KILL are ignored. The system facilities for the user to edit input
       are disabled in noncanonical mode, so that all input characters
       (unless they are special for signal or flow-control purposes)
       are passed to the application program exactly as typed.
       It is up to the application program to give the user ways to edit the
       input, if appropriate. */

    my_serport.c_cc[VMIN]  = 1;            /* specifies the minimum number of bytes that must be available
                                            * in the input queue in order for read to return: 0 = read doesn't block */
    my_serport.c_cc[VTIME] = 10;           /* specifies how long to wait for input before returning,
                                            * in units of 0.1 seconds: 10 = 1 seconds read timeout */

    my_serport.c_iflag &= ~(IXON | IXOFF | IXANY); /* shut off xon/xoff ctrl */


    /* The flags and fields in c_cflag control parameters usually associated with asynchronous serial data transmission */
    /* CLOCAL: If this bit is set, it indicates that the terminal is
     *         connected “locally” and that the modem status lines (such as carrier detect) should be ignored.
     * CREAD:  If this bit is set, input can be read from the terminal.
     *         Otherwise, input is discarded when it arrives. */
    my_serport.c_cflag |= (CLOCAL | CREAD);

    /*shut off parity */
    /* PARENB: If this bit is not set, no parity bit is added to output characters,
     *         and input characters are not checked for correct parity.
     * PARODD: This bit is only useful if PARENB is set.
     *         If PARODD is set, odd parity is used, otherwise even parity is used. */
    my_serport.c_cflag &= ~(PARENB | PARODD);

    /* If this bit is set, two stop bits are used. Otherwise, only one stop bit is used. */
    my_serport.c_cflag &= ~CSTOPB;    /* 1 stop bit */

    /* disable hardware flow control */
    my_serport.c_cflag &= ~CRTSCTS;

    /* set serial port parameters */
    if(tcsetattr(fd_dev,TCSANOW,&my_serport) != 0) {
        return -1;
    }

    /* clear data in input/output queues */
    if(tcflush(fd_dev, TCIOFLUSH) != 0) {
        return -1;
    }
    return 0;
}

int main(int argc, char *argv[])
{

    if (argc != 4) {
        fprintf(stderr,"Usage: %s DEVICE BAUDRATE NUMBER_OF_CHANNELS\n",argv[0]);
        return -1;
    }

    char *end;
    int baudrate = strtol(argv[2],&end,0);

    if (*end != '\0') {
        fprintf(stderr,"Usage: %s DEVICE BAUDRATE NUMBER_OF_CHANNELS\n",argv[0]);
        return -1;
    }
    speed_t baud = B9600;

    switch(baudrate) {
    case 1200:
        baud = B1200;
        break;
    case 2400:
        baud = B2400;
        break;
    case 9600:
        baud = B9600;
        break;
    case 19200:
        baud = B19200;
        break;
    case 57600:
        baud = B57600;
        break;
    case 115200:
        baud = B115200;
        break;
    case 230400:
        baud = B230400;
        break;
    default : {
        fprintf(stderr,"Unsupported baud rate: %d\n",baudrate);
        return -1;
    }
    }


    num_channels = strtol(argv[3],&end,0);

    if (*end != '\0') {
        fprintf(stderr,"Usage: %s DEVICE BAUDRATE NUMBER_OF_CHANNELS\n",argv[0]);
        return -1;
    }
    if (num_channels > MAX_CHANNELS) {
        fprintf(stderr,"%s: Cannot use more than %d channels\n",argv[0],MAX_CHANNELS);
        return -1;
    }

    // Opening PTYs
    for (int i = 0; i < num_channels; i++) {
        channel_masters[i] = posix_openpt(O_RDWR);

        if (channel_masters[i] == -1) {
            fprintf(stderr,"%s: Cannot open channel: %s\n",argv[0],strerror(errno));
            return -1;
        }

        /* Switch echo off etc */
        struct termios param;
        tcgetattr(channel_masters[i], &param);
        cfmakeraw (&param);
        tcsetattr(channel_masters[i], TCSANOW, &param);

        /* Allow slave connection */
        unlockpt(channel_masters[i]);
        grantpt(channel_masters[i]);
        char *pn = ptsname(channel_masters[i]);
        if (!pn) {
            fprintf(stderr,"%s: Cannot open channel: %s\n",argv[0],strerror(errno));
        } else {
            fprintf(stdout,"Channel %d: %s (%d)\n",i,pn,channel_masters[i]);
        }
        /* Open slave, thus allowing close() on it in other progs */
        channel_slaves[i] = open(pn,O_RDONLY);
    }

    // Opening serial dev
    serial = open(argv[1],O_RDWR | O_NOCTTY);

    if (serial == -1) {
        fprintf(stderr,"%s: open() failed: %s\n",argv[0],strerror(errno));
        return -1;
    }

    if (port_setup(serial,baud)) {
        fprintf(stderr,"%s: port setup failed: %s\n",argv[0],strerror(errno));
        return -1;
    }

    // Install sigint handler
    signal(SIGINT,terminate);
    unsigned char serial_prefix;
    unsigned char serial_data;

    // Listening on PTYs
    for(;;) {
        fd_set rx;
        struct timeval tv;
        int retval;
        FD_ZERO(&rx);
        FD_SET(serial, &rx);
        int maxfd = serial;
        for (int i = 0; i < num_channels; i++) {
            FD_SET(channel_masters[i], &rx);
            if (channel_masters[i] > maxfd) maxfd = channel_masters[i];
        }
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        retval = select(maxfd+1,&rx, NULL, NULL, &tv);

        if (retval == -1) {
            fprintf(stderr,"%s: select() failed: %s\n",argv[0],strerror(errno));
            goto END;
        } else if (retval) {
            // Relay serial in data
            if (FD_ISSET(serial,&rx)) {
#ifdef DEBUG
                fprintf(stderr,"Serial byte received.\n");
#endif
                if (read(serial,&serial_prefix,1) != 1 ||
                        read(serial,&serial_data,1)   != 1) {
                    fprintf(stderr,"%s: read() failed: %s\n",argv[0],strerror(errno));
                    goto END;
                } else {
#ifdef DEBUG
                    fprintf(stderr,"Serial input data available from channel %d. Writing to %d\n",serial_prefix,channel_masters[serial_prefix]);
#endif
                    //if (serial_prefix == 0) continue;

                    if (serial_prefix >= num_channels) {
                        fprintf(stderr,"%s: Invalid channel %d\n",argv[0],serial_prefix);
                        goto END;
                    } else if (write(channel_masters[serial_prefix],&serial_data,1) != 1) {
                        fprintf(stderr,"%s: write() failed: %s\n",argv[0],strerror(errno));
                        goto END;
                    }
                }
                continue;
            }
            // Relay serial output data
            for (int i = 0; i < num_channels; i++) {
                if (FD_ISSET(channel_masters[i], &rx)) {
#ifdef DEBUG
                    fprintf(stderr,"Serial output data for channel %d\n",i);
#endif
                    serial_prefix = i;
                    if(read(channel_masters[i],&serial_data,1) != 1) {
                        fprintf(stderr,"%s: read() failed: %s\n",argv[0],strerror(errno));
                        goto END;
                    } else if (write(serial,&serial_prefix,1) != 1 ||
                               write(serial,&serial_data,1) != 1) {
                        fprintf(stderr,"%s: write() failed: %s\n",argv[0],strerror(errno));
                        goto END;
                    }
                }
            }
        }
    }
END:
    terminate(0);
}
