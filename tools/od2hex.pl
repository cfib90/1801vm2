#!/usr/bin/env perl
use strict;
use Getopt::Long;
use POSIX 'strtol';

my $start_address_str = "0";
my $end_address_str   = "65535";
my $start_address;
my $end_address;


GetOptions ('start=s' => \$start_address_str, 'end=s' => \$end_address_str);

my $unp = 0;

($start_address, $unp) = strtol($start_address_str,0);

die "$start_address_str is not a valid address" if ($unp);

($end_address, $unp) = strtol($end_address_str,0);

die "$end_address_str is not a valid address" if ($unp);

if ($end_address < $start_address) {
    print "END must be >= START\n";
    exit -1;
}

if ($start_address%2 || $end_address%2) {
    print "Must start and end on word address\n";
    exit -1;
}

my $ram = [(0)x65536];

while (<>) {
    if ($_ =~ /^\s+([0-7]{6}): ([0-7]{6}) ([0-7]{6})? ([0-7]{6})? ([0-7]{6})?/) {
        my $addr = oct($1);
        @$ram[$addr/2] = oct($2);
        if (defined $3) {
            @$ram[($addr+2)/2] = oct($3);
        }
        if (defined $4) {
            @$ram[($addr+4)/2] = oct($4);
        }
        if (defined $5) {
            @$ram[($addr+6)/2] = oct($5);
        }
    }
}

for (my $addr = $start_address/2; $addr <= $end_address/2; $addr++) {
    printf("%04x\n",@$ram[$addr]);
}

