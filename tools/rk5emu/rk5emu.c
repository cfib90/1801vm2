#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/mman.h>
#include <assert.h>

enum load_state_type {
    ADDRESS = 0x01, TAG = 0x02, READ_WORD = 0x04, WRITE_WORD = 0x08
};

int cont = 1;
char **global_argv;
int global_argc;


/*
 * ### Thank you, SIMH
 */
#define RK_NUMSC        12                              /* sectors/surface */
#define RKDA_V_SECT     0                               /* sector */
#define RKDA_M_SECT     017
#define RKDA_V_TRACK    4                               /* track */
#define RKDA_M_TRACK    0777
#define RKDA_V_CYL      5                               /* cylinder */
#define RKDA_M_CYL      0377
#define RKDA_V_DRIVE    13                              /* drive */
#define RKDA_M_DRIVE    07
#define RKDA_DRIVE      (RKDA_M_DRIVE << RKDA_V_DRIVE)
#define GET_SECT(x)     (((x) >> RKDA_V_SECT) & RKDA_M_SECT)
#define GET_CYL(x)      (((x) >> RKDA_V_CYL) & RKDA_M_CYL)
#define GET_TRACK(x)    (((x) >> RKDA_V_TRACK) & RKDA_M_TRACK)
#define GET_DRIVE(x)    (((x) >> RKDA_V_DRIVE) & RKDA_M_DRIVE)
#define GET_DA(x)       ((GET_TRACK (x) * RK_NUMSC) + GET_SECT (x))

#define BYTES_ON_DISK   2494464

int dsk_fds[8] = {-1,-1,-1,-1,-1,-1,-1,-1};
uint8_t* dsk_ptrs[8] = {NULL};
int fd;

#define DUMPDIR_TEMPLATE "rk5emu_XXXXXX"

char dumpfn[sizeof(DUMPDIR_TEMPLATE)+sizeof("/0.dsk")] = DUMPDIR_TEMPLATE;

void handle_sigint(int sig) {
    close(fd);

#ifdef DEBUG
    fprintf(stderr,"Dumpdir: %s\n",dumpfn);
#endif

    for (int i = 0; i < 8; i++) {
        sprintf(dumpfn+sizeof(DUMPDIR_TEMPLATE)-1,"/%c.dsk",i+'0');
        FILE *dumpfp = fopen(dumpfn,"w");
        if (!dumpfp) {
            fprintf(stderr,"Could not open %s for dump: %s\n",dumpfn,strerror(errno));
            goto END;
        }
        if (fwrite(dsk_ptrs[i],1,BYTES_ON_DISK,dumpfp) != BYTES_ON_DISK || fclose(dumpfp) < 0) {
            fprintf(stderr,"Could write %s for dump: %s\n",dumpfn,strerror(errno));
            goto END;
        }
        fprintf(stderr,"Dumped disk %d (%s) to %s\n",i,(i < global_argc-2) ? global_argv[i+2] : "RAMDISK",dumpfn);
        END:
        free(dsk_ptrs[i]);
    }

    exit(0);
}

int main(int argc, char *argv[])
{
    unsigned char w[2];
    int nw = 0;
    if (argc < 3) {
        fprintf(stderr,"Usage: %s DEVICE DSK0 [DSKn]\n",argv[0]);
        return -1;
    }
    global_argc = argc;
    global_argv = argv;
    mkdtemp(dumpfn);
    
    
    fd = open(argv[1],O_RDWR | O_SYNC);

    for (int i = 2; i < 10; i++) {
        int disk_idx = i-2;
        dsk_ptrs[disk_idx] = calloc(BYTES_ON_DISK,1);
        
        if (!dsk_ptrs[disk_idx]) {
            fprintf(stderr,"Could not allocate memory for file %s: %s\n",argv[i],strerror(errno));
            exit(EXIT_FAILURE);
        }

        if (i < argc) {
            dsk_fds[disk_idx] = open(argv[i],O_RDWR | O_SYNC);
            if (dsk_fds[disk_idx] == -1) {
                fprintf(stderr,"Could not open file %s: %s\n",argv[i],strerror(errno));
                exit(EXIT_FAILURE);
            }

        
            int bytes_read = read(dsk_fds[disk_idx],dsk_ptrs[disk_idx],BYTES_ON_DISK);
            if (bytes_read < BYTES_ON_DISK) {
                fprintf(stderr,"Disk image %s contained only %d bytes",argv[i],bytes_read);
            } else {
                fprintf(stderr,"Attached disk image %s as drive %d\n",argv[i],disk_idx);
            }
            close(dsk_fds[i]);
        } else {
            fprintf(stderr,"RAM Disk for drive %d\n",disk_idx);
        }
    }
    signal(SIGINT,handle_sigint);

    /* Switch echo off etc */
    struct termios param;
    tcgetattr(fd, &param);
    cfmakeraw (&param);
    tcsetattr(fd, TCSANOW, &param);
    tcflush(fd,TCIOFLUSH);

    enum load_state_type state = ADDRESS;
    int len;
    uint16_t bnum = 0;
    uint16_t sector;
    uint16_t  drive;
    uint16_t offset;
    while(cont) {
        uint16_t word;
        uint8_t  byte;

        if (state == ADDRESS || state == WRITE_WORD) {
            int to_read = 2;
            uint8_t *off = w;
            while (to_read > 0) {
                int rv = read(fd,off,to_read);
                if (rv < 0) continue;
                to_read -= rv;
                off += rv;
            }
            word = (((uint16_t)w[1])<<8) | w[0];
#ifdef DEBUG
        printf("Read: %04x, %d\n",word,state);
#endif
        } else if (state == TAG) {
            if(read(fd,w,1) != 1) continue;
            byte = w[0];
#ifdef DEBUG
        printf("Read: %02x, %d\n",w[0],state);
#endif
        }
        uint16_t sect;
        uint16_t surf;
        uint16_t cyl;
        uint16_t track;
        switch (state) {
        case ADDRESS: {
            sect = (word&0xF);
            surf = ((word>>4) & 1);
            cyl  = (word>>5) & 0xFF;
            track = (word>>4) &0xFF;
            assert(sect <= 12);
            assert(cyl <= 302);
            sector = track * 12 + sect;
            drive  = word>>13;
            offset = 0;
            state = TAG;
            fprintf(stderr,"Drive: %d, Cylinder: %d, Surface: %d, Sector: %d\n",drive,cyl,surf,sect);
        } break;
        case TAG: {
            switch(byte) {
                case 0x04: {
                    state = ADDRESS; /* Transfer end */
                } break;
                case 0x05: {
                    state = READ_WORD; /* read word */
                } break;
                case 0x0F: {
                    state = WRITE_WORD; /* write word */
                } break;
            }
        } break;
        case READ_WORD: {
            if (dsk_ptrs[drive]) {
#ifdef LE
                w[0] = dsk_ptrs[drive][sector*512+offset+1];
                w[1] = dsk_ptrs[drive][sector*512+offset];
#else
                w[0] = dsk_ptrs[drive][sector*512+offset];
                w[1] = dsk_ptrs[drive][sector*512+offset+1];
#endif
            } else {
                w[0] = 0xAD;
                w[1] = 0xDE;
            }
            fprintf(stderr,"Reading %d:%d:%d:%d, LBA 0x%08x + offset %d =>  %02x%02x\n",drive,cyl,surf,sect,sector,offset,w[1],w[0]);
            write(fd,w,2);
            offset += 2;
            if (offset == 512) { /* only high density disks */
                sector++;
                offset = 0;
            }
            state = TAG;
        } break;
        case WRITE_WORD: {
            if (dsk_ptrs[drive]) {
                fprintf(stderr,"Writing %d:%d:%d:%d, offset %d => LBA 0x%08x: %02x%02x\n",drive,cyl,surf,sect,offset,sector*512+offset,w[1],w[0]);
#ifdef LE
                dsk_ptrs[drive][sector*512+offset+1]   = w[0];
                dsk_ptrs[drive][sector*512+offset] = w[1];
#else
                dsk_ptrs[drive][sector*512+offset]   = w[0];
                dsk_ptrs[drive][sector*512+offset+1] = w[1];
#endif
                offset += 2;
                if (offset == 512) {  /* only high density disks */
                    sector++;
                    offset = 0;
                }
            }
            state = TAG;
        } break;
        }
    }

END:
    perror("read()");


    return 0;
}
