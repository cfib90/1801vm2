#!/usr/bin/env perl
use strict;
use Getopt::Long;
use POSIX 'strtol';

my $s = "HEAD";
my $addr = 0;
my $len = 0;
my $chk = 0;
my $data;
while (<>) {
    if ($s eq "HEAD") {
        if ($_ =~ "TEXT ADDR=([0-9]+) LEN=([0-9]+)") {
            $addr = strtol($1,8);
            $len  = strtol($2,8) + 6;
            $s    = "DATA";
            $chk  = 1 + 0 + ($addr&0xFF) + (($addr>>8)&0xFF) + ($len&0xFF) + (($len>>8)&0xFF);
            printf("%c%c%c%c%c%c",1,0,$len&0xFF,$len>>8,$addr&0xFF,$addr>>8);
        }
    } elsif ($s eq "DATA") {
        if ($_ =~ "^RLD" || $_ =~ "^ENDMOD") {
            $s = "HEAD";
            $chk &= 0xFF;
            my $nchk = ($chk==0)?0:((256-$chk)&0xFF);
            printf("%c",$nchk);
        } elsif ($_ =~ "TEXT ADDR=([0-9]+) LEN=([0-9]+)") {
            $chk &= 0xFF;
            my $nchk = ($chk==0)?0:((256-$chk)&0xFF);
            printf("%c",$nchk);
            $addr = strtol($1,8);
            $len  = strtol($2,8) + 6;
            $s    = "DATA";
            $chk  = 1 + 0 + ($addr&0xFF) + (($addr>>8)&0xFF) + ($len&0xFF) + (($len>>8)&0xFF);
            printf("%c%c%c%c%c%c",1,0,$len&0xFF,$len>>8,$addr&0xFF,$addr>>8);
        } else {
            if ($_ =~ /^\s+([0-7]{6}): ([0-7]{6}) ([0-7]{6})? ([0-7]{6})? ([0-7]{6})?/) {
                $data = oct($2);
                $chk += ($data&0xFF) + ($data>>8)&0xFF;
                printf("%c%c",$data&0xFF,$data>>8);
                if (defined $3) {
                    $data = oct($3);
                    $chk += ($data&0xFF) + ($data>>8)&0xFF;
                    printf("%c%c",$data&0xFF,$data>>8);
                }
                if (defined $4) {
                    $data = oct($4);
                    $chk += ($data&0xFF) + ($data>>8)&0xFF;
                    printf("%c%c",$data&0xFF,$data>>8);
                }
                if (defined $5) {
                    $data = oct($5);
                    $chk += ($data&0xFF) + ($data>>8)&0xFF;
                    printf("%c%c",$data&0xFF,$data>>8);
                }
            }
        }
    }
}
printf("%c%c",1,0);
printf("%c%c",6,0);
printf("%c%c",1,0);
printf("%c",(256-8)&0xFF);

