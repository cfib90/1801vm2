# 1801vm2 HX8K adapter PCB #

The adapter PCB has the task of supplying the CPU with 5V and to translate
between 5V and 3V3 voltage levels. Level translation is done via GTL2010
FET level shifters with pull-ups on the 5V side.

## BUGS BUGS BUGS ##

There are some bugs and quirks that (If I'll make one) should be fixed in Rev.2:

* The `ar_n` wire is connected to `PIO3_26` on the current PCB. This is bad, because
  the HX8K breakout board has a clock output there. A temporary fix is to run a
  mod wire to `PIO3_19` and cut off `PIO3_26` on the pin header.

* The CPU could use a power switch and/or reset button

* If we use a stabilized 5V PSU, we can omit the LM7805. If we use the LM7805, the
  silkscreen next to it should read 7V-9V instead of 6V-9V.

* Alternatively, the next revision could adapt the CPU to some board that provides
  SRAM (e.g., [icoBoard](http://icoboard.org/)) as the HX8K is really limited in On-chip RAM





